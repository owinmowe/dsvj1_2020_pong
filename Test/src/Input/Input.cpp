#include "Input.h"
#include "External Variables/Extern_Variables.h"

Input checkInput()
{

	Input newInput;

    if (IsKeyPressed(gameplay::currentKeycodes.enter))			    { newInput.enter = true; }
	if (IsKeyPressed(gameplay::currentKeycodes.pause))			    { newInput.pause = true; }
    if (IsKeyPressed(gameplay::currentKeycodes.reset))              { newInput.reset = true; }
    if (IsKeyPressed(gameplay::currentKeycodes.change))             { newInput.change = true; }

    if (IsKeyPressed(gameplay::currentKeycodes.pressed_LeftArrow))  { newInput.pressed_LeftArrow = true; };
    if (IsKeyPressed(gameplay::currentKeycodes.pressed_RightArrow)) { newInput.pressed_RightArrow = true; };
    if (IsKeyPressed(gameplay::currentKeycodes.pressed_UpArrow))    { newInput.pressed_UpArrow = true; };
    if (IsKeyPressed(gameplay::currentKeycodes.pressed_DownArrow))  { newInput.pressed_DownArrow = true; };

    if (IsKeyPressed(gameplay::currentKeycodes.pressed_A))          { newInput.pressed_A = true; };
    if (IsKeyPressed(gameplay::currentKeycodes.pressed_S))          { newInput.pressed_S = true; };
    if (IsKeyPressed(gameplay::currentKeycodes.pressed_D))          { newInput.pressed_D = true; };
    if (IsKeyPressed(gameplay::currentKeycodes.pressed_W))          { newInput.pressed_W = true; };


    if (IsKeyDown(gameplay::currentKeycodes.down_UpArrow))          { newInput.down_UpArrow = true; };
    if (IsKeyDown(gameplay::currentKeycodes.down_DownArrow))        { newInput.down_DownArrow = true; };
    if (IsKeyDown(gameplay::currentKeycodes.down_W))                { newInput.down_W = true; };
    if (IsKeyDown(gameplay::currentKeycodes.down_S))                { newInput.down_S = true; };

    return newInput;

}

int returnKey()
{

	return GetKeyPressed();

}

