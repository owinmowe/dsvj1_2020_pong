#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

struct NEW_KEYCODES
{

	int enter = KEY_ENTER;
	int pause = KEY_P;
	int reset = KEY_R;
	int change = KEY_C;

	int down_UpArrow = KEY_UP;
	int down_DownArrow = KEY_DOWN;
	int down_W = KEY_W;
	int down_S = KEY_S;

	int pressed_A = KEY_A;
	int pressed_S = KEY_S;
	int pressed_D = KEY_D;
	int pressed_W = KEY_W;

	int pressed_LeftArrow = KEY_LEFT;
	int pressed_RightArrow = KEY_RIGHT;
	int pressed_UpArrow = KEY_UP;
	int pressed_DownArrow = KEY_DOWN;

};

struct Input
{

	bool enter = false;
	bool pause = false;
	bool reset = false;
	bool change = false;

	bool down_UpArrow = false;
	bool down_DownArrow = false;
	bool down_W = false;
	bool down_S = false;

	bool pressed_A = false;
	bool pressed_S = false;
	bool pressed_D = false;
	bool pressed_W = false;

	bool pressed_LeftArrow = false;
	bool pressed_RightArrow = false;
	bool pressed_UpArrow = false;
	bool pressed_DownArrow = false;

};

Input checkInput();

int returnKey();

#endif INPUT_H