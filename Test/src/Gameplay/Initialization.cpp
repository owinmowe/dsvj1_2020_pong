#include "Initialization.h"
#include "External Variables/Extern_Variables.h"
#include "Save Positions/save_Positions.h"

namespace config
{

    bool superMode = true;

    int goalPerRound = GOAL_BASE_PER_MATCH;

    int padSpeed = PAD_BASE_SPEED;

    int padSize = PAD_BASE_HEIGHT;

    int ballSize = BALL_BASE_SIZE;

    int hitsForPowerUp = POWERUP_BASE_HITS;

    int timeForRandomPowerUp = POWERUP_BASE_RANDOM_TIME;

}

void matchConfig()
{

    using namespace gameplay;

    currentPongObjects.outerBounds = setOuterBound();

    currentPongObjects.padLeft = setPad(currentPongObjects.padLeft, PAD_POSITION::LEFT);

    currentPongObjects.padRight = setPad(currentPongObjects.padRight, PAD_POSITION::RIGHT);

    currentPongObjects.leftGoal = setGoal(currentPongObjects.outerBounds, currentPongObjects.padLeft);

    currentPongObjects.rightGoal = setGoal(currentPongObjects.outerBounds, currentPongObjects.padRight);

    currentPongObjects.ball = resetBall();

    resetPowerUps();

    resetPowerUpsHits();

}

void initialization()
{

    InitWindow(screen::SCREEN_WIDTH, screen::SCREEN_HEIGHT, "Super Pong");

    InitAudioDevice();

    gameplay::currentGameState.soundVolume = (float)LoadStorageValue(SAVE_POSITION_SOUND_VOLUME) / SAVE_VOLUME_MULTIPLIER;

    gameplay::currentGameState.musicVolume = (float)LoadStorageValue(SAVE_POSITION_MUSIC_VOLUME) / SAVE_VOLUME_MULTIPLIER;

    setSounds(gameplay::currentGameState, audio::gameSounds);

    setMusic(gameplay::currentGameState, audio::gameMusic);

    SetTargetFPS(screen::FRAMES_PER_SECOND);               // Set our game to run at 60 frames-per-second

}

void deInitialization()
{

    UnloadMusicFromMemory(audio::gameMusic);
    UnloadSoundsFromMemory(audio::gameSounds);
    //CloseAudioDevice();
    CloseWindow();        // Close window and OpenGL context

}