#include "Input/Input.h"
#include "Gameplay/Game_State.h"
#include "UI/Menu.h"
#include "Gameplay/PongObjects/Pong_Objects.h"

namespace gameplay
{

	int framesCounter = 0; //Counts from 0 to 60 

	Input input;

	NEW_KEYCODES currentKeycodes;

	GameState currentGameState;

	GameMenus currentGameMenus;

	PongObjects currentPongObjects;

}