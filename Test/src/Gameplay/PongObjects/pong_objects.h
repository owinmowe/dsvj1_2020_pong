#ifndef PONG_OBJECTS_H
#define PONG_OBJECTS_H

#include "Gameplay/PongObjects/Ball/Ball.h"
#include "Gameplay/PongObjects/Pads.h"
#include "Gameplay/PongObjects/Bounds.h"
#include "Gameplay/PongObjects/Power_Ups.h"

struct PongObjects
{

    Ball ball;
    Ball multiball;
    Pad padLeft;
    Pad padRight;
    Bound outerBounds;
    Bound leftGoal;
    Bound rightGoal;
    PowerUpsRandom randomPowerUps;
    PowerUpsPads padsPowerUps;

};

#endif PONG_OBJECTS_H