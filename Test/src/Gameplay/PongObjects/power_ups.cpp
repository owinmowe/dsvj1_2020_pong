#include "Gameplay/PongObjects/power_ups.h"
#include "External Variables/Extern_Variables.h"

using namespace gameplay;

void randomPowerUpControl()
{

    if (!config::superMode) { return; }

    if (currentPongObjects.ball.hitEvent || currentPongObjects.multiball.hitEvent)
    {
        currentPongObjects.padsPowerUps.hitsForNextPowerUp--;
    }

    if(currentPongObjects.padsPowerUps.hitsForNextPowerUp <= 0)
    {
        currentPongObjects.padsPowerUps.hitsForNextPowerUp = config::hitsForPowerUp;

        resetHitsPowerUps();

        currentPongObjects.padLeft.currentPowerUp = (PadPowerUpStatus)GetRandomValue(0, static_cast<int>(PadPowerUpStatus::SIZE) - 1);
        currentPongObjects.padRight.currentPowerUp = (PadPowerUpStatus)GetRandomValue(0, static_cast<int>(PadPowerUpStatus::SIZE) - 1);

        if (currentPongObjects.padLeft.currentPowerUp == PadPowerUpStatus::BIG_PAD)
        {
            currentPongObjects.padsPowerUps.leftBigPadPowerUp = true;
            currentPongObjects.padLeft.powerUpTimer = POWERUP_BIG_PAD_TIME;
            currentPongObjects.padLeft.rec.height *= POWERUP_BIG_PAD_SIZE_MOD;
        }
        else if (currentPongObjects.padLeft.currentPowerUp == PadPowerUpStatus::SHIELD)
        {
            currentPongObjects.padsPowerUps.leftShieldPowerUp.active = true;
            currentPongObjects.padLeft.powerUpTimer = POWERUP_SHIELD_TIME;
        }
        else if (currentPongObjects.padLeft.currentPowerUp == PadPowerUpStatus::SPEED)
        {
            currentPongObjects.padsPowerUps.leftSpeedPowerUp = true;
            currentPongObjects.padLeft.speed *= POWERUP_SPEED_MOD;
            currentPongObjects.padLeft.powerUpTimer = POWERUP_SPEED_TIME;
        }

        if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::BIG_PAD)
        {
            currentPongObjects.padsPowerUps.rightBigPadPowerUp = true;
            currentPongObjects.padRight.powerUpTimer = POWERUP_BIG_PAD_TIME;
            currentPongObjects.padRight.rec.height *= POWERUP_BIG_PAD_SIZE_MOD;
        }
        else if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::SHIELD)
        {
            currentPongObjects.padsPowerUps.rightShieldPowerUp.active = true;
            currentPongObjects.padRight.powerUpTimer = POWERUP_SHIELD_TIME;
        }
        else if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::SPEED)
        {
            currentPongObjects.padsPowerUps.rightSpeedPowerUp = true;
            currentPongObjects.padRight.speed *= POWERUP_SPEED_MOD;
            currentPongObjects.padRight.powerUpTimer = POWERUP_SPEED_TIME;
        }

    }

    if(framesCounter % screen::FRAMES_PER_SECOND == 0 && currentPongObjects.ball.active && !currentGameState.pause)
    {
        currentPongObjects.randomPowerUps.timer--;

        if(currentPongObjects.padLeft.powerUpTimer > 0)
        {
            currentPongObjects.padLeft.powerUpTimer--;
        }
        if (currentPongObjects.padRight.powerUpTimer > 0)
        {
            currentPongObjects.padRight.powerUpTimer--;
        }

    }

    if(currentPongObjects.padLeft.currentPowerUp != PadPowerUpStatus::NONE && currentPongObjects.padLeft.powerUpTimer <= 0)
    {

        if(currentPongObjects.padLeft.currentPowerUp == PadPowerUpStatus::BIG_PAD)
        {
            currentPongObjects.padLeft.rec.height = static_cast<float>(config::padSize);
        }
        else if (currentPongObjects.padLeft.currentPowerUp == PadPowerUpStatus::SPEED)
        {
            currentPongObjects.padLeft.speed = static_cast<float>(config::padSpeed);
        }
        else // (currentPongObjects.padLeft.currentPowerUp == PadPowerUpStatus::SHIELD)
        {
            currentPongObjects.padsPowerUps.leftShieldPowerUp.active = false;
        }
        currentPongObjects.padLeft.currentPowerUp = PadPowerUpStatus::NONE;

    }
    if(currentPongObjects.padRight.currentPowerUp != PadPowerUpStatus::NONE && currentPongObjects.padRight.powerUpTimer <= 0)
    {

        if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::BIG_PAD)
        {
            currentPongObjects.padRight.rec.height = static_cast<float>(config::padSize);
        }
        else if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::SPEED)
        {
            currentPongObjects.padRight.speed = static_cast<float>(config::padSpeed);
        }
        else // (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::SHIELD)
        {
            currentPongObjects.padsPowerUps.rightShieldPowerUp.active = false;
        }
        currentPongObjects.padRight.currentPowerUp = PadPowerUpStatus::NONE;

    }

    if (currentPongObjects.randomPowerUps.currentType == PowerUpsRandomTypes::MULTIBALL_POWERUP)
    {

        secondBallUpdate(currentPongObjects.multiball);

    }
    else if(currentPongObjects.randomPowerUps.currentType == PowerUpsRandomTypes::OBSTACLES_POWERUP)
    {

        objectsUpdate();

    }

    if(currentPongObjects.randomPowerUps.timer < 0)
    {

        PowerUpsRandomTypes previousType = currentPongObjects.randomPowerUps.currentType;

        do
        {
            currentPongObjects.randomPowerUps.currentType = (PowerUpsRandomTypes)GetRandomValue(0, static_cast<int>(PowerUpsRandomTypes::SIZE) - 1);

        } while (previousType == currentPongObjects.randomPowerUps.currentType);

        currentPongObjects.randomPowerUps.timer = config::timeForRandomPowerUp;

        if (currentPongObjects.randomPowerUps.currentType == PowerUpsRandomTypes::MULTIBALL_POWERUP)
        {

            currentPongObjects.multiball = resetBall();

            currentPongObjects.multiball.active = true;

        }

    }

    if(currentPongObjects.leftGoal.hitEvent || currentPongObjects.rightGoal.hitEvent)
    {

        resetPowerUps();

    }

}

void resetHitsPowerUps()
{
    if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::BIG_PAD)
    {
        currentPongObjects.padRight.rec.height = static_cast<float>(config::padSize);
    }
    else if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::SPEED)
    {
        currentPongObjects.padRight.speed = static_cast<float>(config::padSpeed);
    }
    else if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::SHIELD)
    {
        currentPongObjects.padsPowerUps.rightShieldPowerUp.active = false;
    }

    if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::BIG_PAD)
    {
        currentPongObjects.padRight.rec.height = static_cast<float>(config::padSize);
    }
    else if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::SPEED)
    {
        currentPongObjects.padRight.speed = static_cast<float>(config::padSpeed);
    }
    else if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::SHIELD)
    {
        currentPongObjects.padsPowerUps.rightShieldPowerUp.active = false;
    }
}

void resetPowerUps()
{

    currentPongObjects.randomPowerUps.timer = config::timeForRandomPowerUp;
    currentPongObjects.randomPowerUps.currentType = PowerUpsRandomTypes::NO_POWERUP;

    currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE1.rec.width = POWERUP_OBSTACLE_WIDTH;
    currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE1.rec.height = POWERUP_OBSTACLE_HEIGHT;
    currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE1.rec.x = screen::SCREEN_WIDTH * 3 / 8;
    currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE1.rec.y = screen::SCREEN_HEIGHT_EIGHT;
    currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE1.directionUp = false;

    currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE2.rec.width = POWERUP_OBSTACLE_WIDTH;;
    currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE2.rec.height = POWERUP_OBSTACLE_HEIGHT;
    currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE2.rec.x = screen::SCREEN_WIDTH * 5 / 8 - currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE2.rec.width;
    currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE2.rec.y = screen::SCREEN_HEIGHT * 7 / 8 - currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE2.rec.height - matchOffsetPanel;
    currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE2.directionUp = true;

    currentPongObjects.padsPowerUps.leftShieldPowerUp.rec = currentPongObjects.leftGoal.rec;
    currentPongObjects.padsPowerUps.leftShieldPowerUp.rec.width = POWERUP_SHIELD_SIZE;
    currentPongObjects.padsPowerUps.leftShieldPowerUp.rec.x += currentPongObjects.leftGoal.rec.width + POWERUP_SHIELD_OFFSET;

    currentPongObjects.padsPowerUps.rightShieldPowerUp.rec = currentPongObjects.rightGoal.rec;
    currentPongObjects.padsPowerUps.rightShieldPowerUp.rec.width = POWERUP_SHIELD_SIZE;
    currentPongObjects.padsPowerUps.rightShieldPowerUp.rec.x -= POWERUP_SHIELD_OFFSET + POWERUP_SHIELD_SIZE;

    currentPongObjects.padLeft.rec.height = static_cast<float>(config::padSize);
    currentPongObjects.padRight.rec.height = static_cast<float>(config::padSize);
    currentPongObjects.padLeft.speed = static_cast<float>(config::padSpeed);
    currentPongObjects.padRight.speed = static_cast<float>(config::padSpeed);
    currentPongObjects.padsPowerUps.leftShieldPowerUp.active = false;
    currentPongObjects.padsPowerUps.rightShieldPowerUp.active = false;

    currentPongObjects.padLeft.currentPowerUp = PadPowerUpStatus::NONE;
    currentPongObjects.padRight.currentPowerUp = PadPowerUpStatus::NONE;

}

void resetPowerUpsHits() 
{
    currentPongObjects.padsPowerUps.hitsForNextPowerUp = config::hitsForPowerUp;
}

void objectsUpdate() 
{

    Obstacle* obstacle1 = &gameplay::currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE1;

    Obstacle* obstacle2 = &gameplay::currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE2;

    if(obstacle1->directionUp)
    {

        obstacle1->rec.y -= POWERUP_OBSTACLE_SPEED;

        if(obstacle1->rec.y < currentPongObjects.outerBounds.size)
        {
            obstacle1->rec.y = static_cast<float>(currentPongObjects.outerBounds.size);
            obstacle1->directionUp = false;
        }

    }
    else
    {
        obstacle1->rec.y += POWERUP_OBSTACLE_SPEED;
        if (obstacle1->rec.y + obstacle1->rec.height > currentPongObjects.outerBounds.rec.height - currentPongObjects.outerBounds.size)
        {

            obstacle1->rec.y = screen::SCREEN_HEIGHT - matchOffsetPanel - currentPongObjects.outerBounds.size - obstacle1->rec.height;
            obstacle1->directionUp = true;

        }
    }

    if (obstacle2->directionUp)
    {

        obstacle2->rec.y -= POWERUP_OBSTACLE_SPEED;

        if (obstacle2->rec.y < currentPongObjects.outerBounds.size)
        {

            obstacle2->rec.y = static_cast<float>(currentPongObjects.outerBounds.size);
            obstacle2->directionUp = false;

        }

    }
    else
    {

        obstacle2->rec.y += POWERUP_OBSTACLE_SPEED;
        if (obstacle2->rec.y + obstacle2->rec.height > currentPongObjects.outerBounds.rec.height - currentPongObjects.outerBounds.size)
        {

            obstacle2->rec.y = screen::SCREEN_HEIGHT - matchOffsetPanel - currentPongObjects.outerBounds.size - obstacle2->rec.height;
            obstacle2->directionUp = true;

        }

    }

}