#include "Bounds.h"
#include "External Variables/extern_variables.h"

Bound setOuterBound()
{

    Bound outerBounds;
    outerBounds.rec.x = 0;
    outerBounds.rec.y = 0;
    outerBounds.size = 10;
    outerBounds.rec.width = screen::SCREEN_WIDTH;
    outerBounds.rec.height = screen::SCREEN_HEIGHT - matchOffsetPanel;
    outerBounds.color = BLACK;
    return outerBounds;

}

Bound setGoal(Bound outerBounds, Pad pad)
{

    if(pad.padPos == PAD_POSITION::LEFT)
    {

        Bound leftGoal;
        leftGoal.color = Fade(pad.color, 0.25f);
        leftGoal.rec.x = outerBounds.rec.x;
        leftGoal.rec.y = outerBounds.rec.y;
        leftGoal.rec.height = outerBounds.rec.height;
        leftGoal.rec.width = pad.rec.x - pad.rec.width;
        return leftGoal;

    }
    else // if(pad.padPos == PAD_POSITION::RIGHT)
    {

        Bound rightGoal;
        rightGoal.color = Fade(pad.color, 0.25f);
        rightGoal.rec.x = pad.rec.x + pad.rec.width * 2;
        rightGoal.rec.y = outerBounds.rec.y - outerBounds.size;
        rightGoal.rec.height = outerBounds.rec.height;
        rightGoal.rec.width = rightGoal.rec.x - pad.rec.width;
        return rightGoal;

    }

}