#ifndef POWER_UPS_H
#define POWER_UPS_H

#include "raylib.h"

enum class PowerUpsRandomTypes { NO_POWERUP, MULTIBALL_POWERUP, OBSTACLES_POWERUP, INVERT_VELOCITY_POWERUP, INVERT_CONTROLS_POWERUP, SIZE };

const int POWERUP_BASE_RANDOM_TIME = 10;

const int POWERUP_MIN_RANDOM_TIME = 5;

const int POWERUP_MAX_RANDOM_TIME = 15;


const int POWERUP_OBSTACLE_WIDTH = 50;

const int POWERUP_OBSTACLE_HEIGHT = 100;

const int POWERUP_OBSTACLE_SPEED = 5;

struct Obstacle
{
	bool colided = false;
	bool directionUp = false;
	Rectangle rec{0,0,0,0};
};

struct PowerUpsRandom
{

	PowerUpsRandomTypes currentType;

	Obstacle POWERUP_RANDOM_OBSTACLE1;

	Obstacle POWERUP_RANDOM_OBSTACLE2;

	int timer = POWERUP_BASE_RANDOM_TIME;

};


enum class PadPowerUpStatus{ NONE, BIG_PAD, SHIELD, SPEED, SIZE};

struct Shield
{
	bool active = false;
	Rectangle rec;
};

const int POWERUP_BASE_HITS = 10;

const int POWERUP_MIN_HITS = 5;

const int POWERUP_MAX_HITS = 15;


const int POWERUP_SHIELD_TIME = 10;

const int POWERUP_SHIELD_OFFSET = 10;

const int POWERUP_SHIELD_SIZE = 10;

const float POWERUP_SHIELD_FADE_COLOR = 0.75;

const int POWERUP_SPEED_TIME = 10;

const float POWERUP_SPEED_MOD = 1.5;


const float POWERUP_BIG_PAD_SIZE_MOD = 1.25;

const int POWERUP_BIG_PAD_TIME = 10;


struct PowerUpsPads
{

	int hitsForNextPowerUp = POWERUP_BASE_HITS;

	Shield leftShieldPowerUp;

	Shield rightShieldPowerUp;

	bool leftBigPadPowerUp;

	bool rightBigPadPowerUp;

	bool leftSpeedPowerUp;

	bool rightSpeedPowerUp;

};

void randomPowerUpControl();

void resetHitsPowerUps();

void resetPowerUps();

void resetPowerUpsHits();

void objectsUpdate();

#endif POWER_UPS_H

