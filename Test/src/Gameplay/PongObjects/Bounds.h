#ifndef BOUNDS_H
#define BOUNDS_H

#include "raylib.h"
#include "Pads.h"

struct Bound
{

    Rectangle rec = { 0, 0, 0, 0 };
    int size = 10;
    Color color = { 0, 0, 0, 0 };
    bool hitEvent = false;

};

Bound setOuterBound();

Bound setGoal(Bound outerBounds, Pad pad);

#endif BOUNDS_H