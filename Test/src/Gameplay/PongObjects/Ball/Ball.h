#ifndef BALL_H
#define BALL_H

#include "raylib.h"
#include "Gameplay/PongObjects/Pads.h"
#include "Input/Input.h"
#include "Gameplay/PongObjects/Ball/Trail.h"

const int BALL_MAX_SPEED = 20;

const int BALL_BASE_SIZE = 10;
const int BALL_MIN_SIZE = 5;
const int BALL_MAX_SIZE = 15;

struct Ball
{

    Vector2 startingPosition = { 0, 0 };
    Vector2 position = { 0, 0 };
    Vector2 currentVelocity = { 0, 0 };
    float speed = 4;
    float radius = 10;
    bool active = false;
    bool goalEvent = false;
    bool hitEvent = false;
    bool trailActive = false;
    int trailStartSpeed = 6;
    TrailParticle fullTrail[trailMaxParticlesNumber];
    bool padSideCollision = false;
    Color color = { 0, 0, 0, 0 };

};

Ball resetBall();

void updateBall();

void acelerateBall();

void secondBallUpdate(Ball& ball);

#endif BALL_H