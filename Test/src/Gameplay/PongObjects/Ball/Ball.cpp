#include"Gameplay/PongObjects/Ball/Ball.h"
#include "External Variables/Extern_Variables.h"

Ball resetBall()
{

    Ball ball;
    ball.radius = static_cast<float>(config::ballSize);
    ball.startingPosition = { (float)screen::SCREEN_WIDTH_HALF - ball.radius / 2, (float)screen::SCREEN_HEIGHT_HALF - matchOffsetPanel / 2 - ball.radius / 2  };
    ball.position = ball.startingPosition;
    ball.speed = 4;
    ball.trailActive = false;

    PAD_POSITION padStart = (PAD_POSITION)(GetRandomValue(0, static_cast<int>(PAD_POSITION::SIZE) - 1));

    switch (padStart)
    {

    case PAD_POSITION::LEFT:
        ball.color = gameplay::currentPongObjects.padLeft.color;
        ball.currentVelocity = { ball.speed, ball.speed };
        break;

    case PAD_POSITION::RIGHT:
        ball.color = gameplay::currentPongObjects.padRight.color;
        ball.currentVelocity = { -ball.speed, -ball.speed };
        break;

    default:
        break;
    }

    ball.active = false;

    return ball;

}

void updateBall()
{

    using namespace gameplay;

    if (currentGameState.pause) { return; }

    if(currentPongObjects.ball.goalEvent)
    {

        currentPongObjects.ball = resetBall();
        currentPongObjects.multiball = resetBall();

    }
    else if (input.enter && !currentPongObjects.ball.active && !currentGameState.rightRoundWon && !currentGameState.leftRoundWon)
    {

        currentPongObjects.ball.active = true;

    }
    else if (currentPongObjects.ball.active)
    {

        currentPongObjects.ball.position.x += currentPongObjects.ball.currentVelocity.x;
        currentPongObjects.ball.position.y += currentPongObjects.ball.currentVelocity.y;

    }

    if (!currentPongObjects.ball.trailActive)
    {

        for (int i = 0; i < trailMaxParticlesNumber; i++)
        {

            if (currentPongObjects.ball.fullTrail[i].active)
            {

                currentPongObjects.ball.fullTrail[i].life--;

                if (currentPongObjects.ball.fullTrail[i].life <= 0)
                {

                    currentPongObjects.ball.fullTrail[i].active = false;

                }
                else
                {

                    currentPongObjects.ball.fullTrail[i].color = Fade(currentPongObjects.ball.color, (float)currentPongObjects.ball.fullTrail[i].life / (float)maxParticleLife);
                    currentPongObjects.ball.fullTrail[i].radius *= (float)currentPongObjects.ball.fullTrail[i].life / (float)maxParticleLife;

                }

            }

        }

    }
    else
    {

        for (int i = 0; i < trailMaxParticlesNumber; i++)
        {

            if (!currentPongObjects.ball.fullTrail[i].active)
            {

                currentPongObjects.ball.fullTrail[i].life = maxParticleLife;
                currentPongObjects.ball.fullTrail[i].radius = currentPongObjects.ball.radius;
                currentPongObjects.ball.fullTrail[i].position = currentPongObjects.ball.position;
                currentPongObjects.ball.fullTrail[i].active = true;
                break;

            }

        }

        for (int i = 0; i < trailMaxParticlesNumber; i++)
        {
            if (currentPongObjects.ball.fullTrail[i].active)
            {

                currentPongObjects.ball.fullTrail[i].life--;

                if (currentPongObjects.ball.fullTrail[i].life <= 0)
                {

                    currentPongObjects.ball.fullTrail[i].active = false;

                }
                else
                {

                    currentPongObjects.ball.fullTrail[i].color = Fade(currentPongObjects.ball.color, (float)currentPongObjects.ball.fullTrail[i].life / (float)maxParticleLife);
                    currentPongObjects.ball.fullTrail[i].radius *= (float)currentPongObjects.ball.fullTrail[i].life / (float)maxParticleLife;

                }

            }
        }

    }

}

void acelerateBall()
{

    using namespace gameplay;

    if (currentPongObjects.ball.hitEvent)
    {

        if(currentPongObjects.ball.currentVelocity.x > 0 && currentPongObjects.ball.currentVelocity.x < BALL_MAX_SPEED)
        {

            currentPongObjects.ball.currentVelocity.x++;

        }
        else if(currentPongObjects.ball.currentVelocity.x < 0 && currentPongObjects.ball.currentVelocity.x > BALL_MAX_SPEED)
        {

            currentPongObjects.ball.currentVelocity.x--;

        }

        if (currentPongObjects.ball.currentVelocity.y > 0 && currentPongObjects.ball.currentVelocity.y < BALL_MAX_SPEED)
        {

            currentPongObjects.ball.currentVelocity.y++;

        }
        else if (currentPongObjects.ball.currentVelocity.y < 0 && currentPongObjects.ball.currentVelocity.y > BALL_MAX_SPEED)
        {

            currentPongObjects.ball.currentVelocity.y--;

        }

        if (!currentPongObjects.ball.trailActive && currentPongObjects.ball.currentVelocity.x > currentPongObjects.ball.trailStartSpeed)
        {

            currentPongObjects.ball.trailActive = true;

        }

    }

}

void secondBallUpdate(Ball& ball)
{
    if (gameplay::currentGameState.pause) { return; }

    if (ball.goalEvent)
    {

        gameplay::currentPongObjects.ball = resetBall();
        ball = resetBall();

    }
    else if (ball.active)
    {

        ball.position.x += ball.currentVelocity.x;
        ball.position.y += ball.currentVelocity.y;

    }

}