#ifndef TRAIL_H
#define TRAIL_H

#include "raylib.h"

const int maxParticleLife = 50;

const int trailMaxParticlesNumber = 200; // Es una pool, mientras supere el framerate/maxParticleLife alcanza

struct TrailParticle
{

    int life = maxParticleLife;
    Vector2 position{ 0,0 };
    Color color = { 0,0,0,0 };
    float radius = 0;
    bool active = false;

};

#endif TRAIL_H