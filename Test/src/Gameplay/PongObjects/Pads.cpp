#include "Pads.h"
#include "External Variables/Extern_Variables.h"

void aiControl(Pad& pad, int tolerance)
{
	
	Ball currentBallToTrack = gameplay::currentPongObjects.ball;

	if(gameplay::currentPongObjects.multiball.active)
	{
		if (pad.padPos == PAD_POSITION::LEFT && gameplay::currentPongObjects.ball.position.x > gameplay::currentPongObjects.multiball.position.x)
		{
			currentBallToTrack = gameplay::currentPongObjects.multiball;
		}
		else if(pad.padPos == PAD_POSITION::RIGHT && gameplay::currentPongObjects.ball.position.x < gameplay::currentPongObjects.multiball.position.x)
		{
			currentBallToTrack = gameplay::currentPongObjects.multiball;
		}
		else
		{
			currentBallToTrack = gameplay::currentPongObjects.ball;
		}
	}

	if (currentBallToTrack.position.y < (pad.rec.y + pad.rec.height / 2) - tolerance)
	{

		pad.rec.y -= pad.speed;

	}
	else if (currentBallToTrack.position.y > (pad.rec.y + pad.rec.height / 2) + tolerance)
	{

		pad.rec.y += pad.speed;

	}

}

Pad setPad(Pad pad, PAD_POSITION padPos)
{

	if(padPos == PAD_POSITION::LEFT)
	{

		Pad padLeft;
		padLeft.padPos = padPos;
		padLeft.rec.height = static_cast<float>(config::padSize);
		padLeft.rec.width = static_cast<float>(config::padSize) / PAD_SIZE_RELATION;
		padLeft.speed = static_cast<float>(config::padSpeed);
		padLeft.color = pad.color;
		padLeft.rec.x = (screen::SCREEN_WIDTH_EIGHT) - (padLeft.rec.width / 2);
		padLeft.rec.y = (screen::SCREEN_HEIGHT_HALF - matchOffsetPanel / 2) - (padLeft.rec.height / 2);
		padLeft.control = pad.control;
		return padLeft;

	}
	else // if(padPos == PAD_POSITION::RIGHT)
	{

		Pad padRight;
		padRight.padPos = padPos;
		padRight.rec.height = static_cast<float>(config::padSize);
		padRight.rec.width = static_cast<float>(config::padSize / PAD_SIZE_RELATION);
		padRight.speed = static_cast<float>(config::padSpeed);
		padRight.color = pad.color;
		padRight.rec.x = (screen::SCREEN_WIDTH * 7 / 8) - (padRight.rec.width / 2);
		padRight.rec.y = (screen::SCREEN_HEIGHT_HALF - matchOffsetPanel / 2) - (padRight.rec.height / 2);
		padRight.control = pad.control;
		return padRight;

	}

}

void updatePads()
{

	using namespace gameplay;

	if (currentGameState.pause) { return; }

	if(currentPongObjects.padLeft.control == CONTROL::AI_EASY)
	{

		aiControl(currentPongObjects.padLeft, AI_EASY_TOLERANCE);

	}
	else if(currentPongObjects.padLeft.control == CONTROL::AI_NORMAL)
	{

		aiControl(currentPongObjects.padLeft, AI_NORMAL_TOLERANCE);

	}
	else if (currentPongObjects.padLeft.control == CONTROL::AI_HARD)
	{

		aiControl(currentPongObjects.padLeft, AI_HARD_TOLERANCE);

	}
	else
	{

		if(currentPongObjects.randomPowerUps.currentType == PowerUpsRandomTypes::INVERT_CONTROLS_POWERUP)
		{
			if (input.down_W) currentPongObjects.padLeft.rec.y += currentPongObjects.padLeft.speed;
			if (input.down_S) currentPongObjects.padLeft.rec.y -= currentPongObjects.padLeft.speed;
		}
		else
		{
			if (input.down_W) currentPongObjects.padLeft.rec.y -= currentPongObjects.padLeft.speed;
			if (input.down_S) currentPongObjects.padLeft.rec.y += currentPongObjects.padLeft.speed;
		}

	}

	if(currentPongObjects.padRight.control == CONTROL::AI_EASY)
	{

		aiControl(currentPongObjects.padRight, AI_EASY_TOLERANCE);

	}
	else if(currentPongObjects.padRight.control == CONTROL::AI_NORMAL)
	{

		aiControl(currentPongObjects.padRight, AI_NORMAL_TOLERANCE);

	}
	else if (currentPongObjects.padRight.control == CONTROL::AI_HARD)
	{

		aiControl(currentPongObjects.padRight, AI_HARD_TOLERANCE);

	}
	else
	{
		if (currentPongObjects.randomPowerUps.currentType == PowerUpsRandomTypes::INVERT_CONTROLS_POWERUP)
		{
			if (input.down_UpArrow) currentPongObjects.padRight.rec.y += currentPongObjects.padRight.speed;
			if (input.down_DownArrow) currentPongObjects.padRight.rec.y -= currentPongObjects.padRight.speed;
		}
		else
		{
			if (input.down_UpArrow) currentPongObjects.padRight.rec.y -= currentPongObjects.padRight.speed;
			if (input.down_DownArrow) currentPongObjects.padRight.rec.y += currentPongObjects.padRight.speed;
		}

	}

}
