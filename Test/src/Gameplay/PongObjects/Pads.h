#ifndef PADS_H
#define PADS_H

#include "raylib.h"
#include "Input/Input.h"
#include "Gameplay/PongObjects/Power_Ups.h"

const int PAD_WIDTH = 25;
const int PAD_SIZE_RELATION = 4;

const int PAD_BASE_HEIGHT = 100;
const int PAD_MIN_HEIGHT = 50;
const int PAD_MAX_HEIGHT = 150;

const int PAD_BASE_SPEED = 8;
const int PAD_MIN_SPEED = 5;
const int PAD_MAX_SPEED = 15;

const int AI_EASY_TOLERANCE = 50;
const int AI_NORMAL_TOLERANCE = 30;
const int AI_HARD_TOLERANCE = 5;

enum class PAD_POSITION{LEFT, RIGHT, SIZE};

enum class CONTROL{PLAYER, AI_EASY, AI_NORMAL, AI_HARD, SIZE};

struct Pad
{

    PAD_POSITION padPos = PAD_POSITION::LEFT;
    Rectangle rec = { 0, 0, 0, 0 };
    float speed = 4;
    Color color = BLUE;
    PadPowerUpStatus currentPowerUp = PadPowerUpStatus::NONE;
    int powerUpTimer = 0;
    CONTROL control = CONTROL::PLAYER;

};

Pad setPad(Pad pad, PAD_POSITION padPos);

void updatePads();

#endif PADS_H
