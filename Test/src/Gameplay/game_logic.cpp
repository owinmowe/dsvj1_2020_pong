#include "Game_Logic.h"
#include "External Variables/extern_variables.h"
#include "Save Positions/save_Positions.h"

using namespace gameplay;

void checkGoals()
{

	if(currentPongObjects.leftGoal.hitEvent)
	{

		currentGameState.currentLeftScore++;

	}
	else if(currentPongObjects.rightGoal.hitEvent)
	{

		currentGameState.currentRightScore++;

	}

	if(currentGameState.currentLeftScore >= config::goalPerRound && !currentGameState.rightRoundWon)
	{

		currentGameState.rightRoundWon = true;
		currentGameState.winAnimationCounter = screen::FRAMES_PER_SECOND * ROUND_WIN_DELAY_SECONDS;

		currentGameState.currentLeftRoundScore++;
		SaveStorageValue(SAVE_POSITION_LEFT_SCORE, currentGameState.currentLeftRoundScore);
		currentGameState.currentLeftScore = 0;
		currentGameState.currentRightScore = 0;

	}
	else if(currentGameState.currentRightScore >= config::goalPerRound && !currentGameState.leftRoundWon)
	{

		currentGameState.leftRoundWon = true;
		currentGameState.winAnimationCounter = screen::FRAMES_PER_SECOND * ROUND_WIN_DELAY_SECONDS;

		currentGameState.currentRightRoundScore++;
		SaveStorageValue(SAVE_POSITION_RIGHT_SCORE, currentGameState.currentRightRoundScore);
		currentGameState.currentLeftScore = 0;
		currentGameState.currentRightScore = 0;

	}

}

void resetScore()
{

	currentGameState.currentLeftRoundScore = 0;
	currentGameState.currentRightRoundScore = 0;
	SaveStorageValue(SAVE_POSITION_LEFT_SCORE, currentGameState.currentLeftRoundScore);
	SaveStorageValue(SAVE_POSITION_RIGHT_SCORE, currentGameState.currentRightRoundScore);

}