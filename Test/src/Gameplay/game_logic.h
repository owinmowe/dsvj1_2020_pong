#ifndef GAME_LOGIC_H
#define GAME_LOGIC_H

#include "Gameplay/PongObjects/Bounds.h"
#include "Gameplay/Game_State.h"

const int GOAL_BASE_PER_MATCH = 5;

const int GOAL_MIN_PER_MATCH = 2;

const int GOAL_MAX_PER_MATCH = 10;

const int ROUND_WIN_DELAY_SECONDS = 4;

void checkGoals();

void resetScore();

#endif GAME_LOGIC_H

