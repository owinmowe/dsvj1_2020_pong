#ifndef UPDATE_H
#define UPDATE_H

#include "Scenes/main_menu.h"
#include "Scenes/credits.h"
#include "Scenes/match.h"
#include "Scenes/options.h"
#include "Scenes/selection_screen.h"

void update();

void frameCounterLogic();

void sceneChangeLogic();

#endif UPDATE_H
