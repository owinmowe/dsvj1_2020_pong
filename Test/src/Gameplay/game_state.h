#ifndef GAME_STATE_H
#define GAME_STATE_H

enum class GAME_MENU{MAIN_MENU, OPTIONS, SELECTION_SCREEN, CREDITS, IN_MATCH, NONE};

struct GameState
{

    GAME_MENU currentGameMenu = GAME_MENU::NONE;
    GAME_MENU nextGameMenu = GAME_MENU::MAIN_MENU;
    int winAnimationCounter = 0;
    bool gameOn = true;
    bool pause = false;
    float soundVolume = .3f;
    float musicVolume = .3f;
    int currentLeftScore = 0;
    int currentRightScore = 0;
    int currentLeftRoundScore = 0;
    bool rightRoundWon = false;
    int currentRightRoundScore = 0;
    bool leftRoundWon = false;

};

#endif GAME_STATE_H