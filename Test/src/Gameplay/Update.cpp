#include "Update.h"
#include "External Variables/extern_variables.h"

using namespace gameplay;

void update()
{

    frameCounterLogic();

    sceneChangeLogic();

    switch (currentGameState.currentGameMenu)
    {

    case GAME_MENU::MAIN_MENU:

        scenes::main_menu::update();

        break;

    case GAME_MENU::OPTIONS:

        scenes::options::update();

        break;

    case GAME_MENU::CREDITS:

        scenes::credits::update();

        break;

    case GAME_MENU::SELECTION_SCREEN:

        scenes::selection_screen::update();

        break;

    case GAME_MENU::IN_MATCH:

        scenes::match::update();

        break;

    default:
        break;
    }
}

void frameCounterLogic()
{
    framesCounter++;

    if (framesCounter >= screen::FRAMES_PER_SECOND)
    {
        framesCounter = 0;
    }
}

void sceneChangeLogic()
{
    if (currentGameState.currentGameMenu != currentGameState.nextGameMenu)
    {

        switch (currentGameState.currentGameMenu)
        {
        case GAME_MENU::MAIN_MENU:
            scenes::main_menu::deInit();
            break;
        case GAME_MENU::OPTIONS:
            scenes::options::deInit();
            break;
        case GAME_MENU::SELECTION_SCREEN:
            scenes::selection_screen::deInit();
            break;
        case GAME_MENU::CREDITS:
            scenes::credits::deInit();
            break;
        case GAME_MENU::IN_MATCH:
            scenes::match::deInit();
            break;
        case GAME_MENU::NONE:
            break;
        default:
            break;
        }

        switch (currentGameState.nextGameMenu)
        {
        case GAME_MENU::MAIN_MENU:
            scenes::main_menu::init();
            break;
        case GAME_MENU::OPTIONS:
            scenes::options::init();
            break;
        case GAME_MENU::SELECTION_SCREEN:
            scenes::selection_screen::init();
            break;
        case GAME_MENU::CREDITS:
            scenes::credits::init();
            break;
        case GAME_MENU::IN_MATCH:
            scenes::match::init();
            break;
        case GAME_MENU::NONE:
            break;
        default:
            break;
        }

        currentGameState.currentGameMenu = currentGameState.nextGameMenu;

    }
}

