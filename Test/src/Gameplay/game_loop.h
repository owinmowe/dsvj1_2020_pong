#ifndef GAME_LOOP_H
#define GAME_LOOP_H

#include "raylib.h"
#include "External Variables/Extern_Variables.h"
#include "Gameplay/Initialization.h"
#include "Render/Render.h"
#include "Gameplay/Update.h"

void RunGame();

#endif GAME_LOOP_H