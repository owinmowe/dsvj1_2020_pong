#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"
#include "Gameplay/Game_State.h"

struct GameSounds
{

	Sound padHitSounds;
	Sound padHitFastSounds;
	Sound padHitVeryFastSounds;
	Sound goalSound;
	Sound gameOverSound;
	Sound menuMoveSound;
	Sound menuSelectSound;

};

struct GameMusic
{

	Music MatchMusic;
	Music MenuMusic;

};

void setSounds(GameState currentGameState, GameSounds& gameSounds);

void setMusic(GameState currentGameState, GameMusic& gameMusic);

void MenuAudio(bool moveEvent, bool selectEvent);

void GameAudio();

void UnloadSoundsFromMemory(GameSounds& gameSounds);

void UnloadMusicFromMemory(GameMusic& gameMusic);

const char PAD_HIT_SOUND_FILE_PATH[] = "audio/sounds/padHitSound.wav";
const char PAD_HIT_FAST_SOUND_FILE_PATH[] = "audio/sounds/padHitFastSound.wav";
const char PAD_HIT_VERY_FAST_SOUND_FILE_PATH[] = "audio/sounds/padHitVeryFastSound.wav";
const char GOAL_SOUND_FILE_PATH[] = "audio/sounds/goalSound.wav";
const char GAMEOVER_SOUND_FILE_PATH[] = "audio/sounds/gameOver.wav";
const char MENU_MOVE_SOUND_FILE_PATH[] = "audio/sounds/menuMoveSound.wav";
const char MENU_SELECT_SOUND_FILE_PATH[] = "audio/sounds/menuSelectSound.wav";

const char MATCH_MUSIC_FILE_PATH[] = "audio/music/matchMusic.mp3";
const char MENU_MUSIC_FILE_PATH[] = "audio/music/menuMusic.mp3";

const int PAD_HIT_VERY_FAST_SOUND_SPEED = 10;

#endif AUDIO_H