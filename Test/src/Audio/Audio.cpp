#include "Audio.h"
#include "External Variables/extern_variables.h"
#include "Save Positions/save_Positions.h"

namespace audio
{

    GameMusic gameMusic;

    GameSounds gameSounds;

    bool changeVolume;

}

void SetAllSoundVolume(GameState currentGameState, GameSounds gameSounds)
{
    SetSoundVolume(gameSounds.padHitSounds, currentGameState.soundVolume);
    SetSoundVolume(gameSounds.padHitFastSounds, currentGameState.soundVolume);
    SetSoundVolume(gameSounds.padHitVeryFastSounds, currentGameState.soundVolume);
    SetSoundVolume(gameSounds.goalSound, currentGameState.soundVolume);
    SetSoundVolume(gameSounds.gameOverSound, currentGameState.soundVolume);
    SetSoundVolume(gameSounds.menuMoveSound, currentGameState.soundVolume);
    SetSoundVolume(gameSounds.menuSelectSound, currentGameState.soundVolume);
}

void setSounds(GameState currentGameState, GameSounds& gameSounds)
{

    gameSounds.padHitSounds = LoadSound(PAD_HIT_SOUND_FILE_PATH);
    gameSounds.padHitFastSounds = LoadSound(PAD_HIT_FAST_SOUND_FILE_PATH);
    gameSounds.padHitVeryFastSounds = LoadSound(PAD_HIT_VERY_FAST_SOUND_FILE_PATH);
    gameSounds.goalSound = LoadSound(GOAL_SOUND_FILE_PATH);
    gameSounds.gameOverSound = LoadSound(GAMEOVER_SOUND_FILE_PATH);
    gameSounds.menuMoveSound = LoadSound(MENU_MOVE_SOUND_FILE_PATH);
    gameSounds.menuSelectSound = LoadSound(MENU_SELECT_SOUND_FILE_PATH);

    SetAllSoundVolume(currentGameState, gameSounds);

}

void SetAllMusicVolume(GameState currentGameState, GameMusic gameMusic)
{

    SetMusicVolume(gameMusic.MatchMusic, currentGameState.musicVolume);

    SetMusicVolume(gameMusic.MenuMusic, currentGameState.musicVolume);

}

void setMusic(GameState currentGameState, GameMusic& gameMusic)
{

    gameMusic.MatchMusic = LoadMusicStream(MATCH_MUSIC_FILE_PATH);

    gameMusic.MenuMusic = LoadMusicStream(MENU_MUSIC_FILE_PATH);

    SetAllMusicVolume(currentGameState, gameMusic);

}

void GameAudio()
{

    using namespace gameplay;
    using namespace audio;

    if (currentGameState.pause) { return; }

    UpdateMusicStream(gameMusic.MatchMusic);

    if((currentPongObjects.ball.hitEvent && (currentPongObjects.ball.currentVelocity.x > PAD_HIT_VERY_FAST_SOUND_SPEED || currentPongObjects.ball.currentVelocity.x < -PAD_HIT_VERY_FAST_SOUND_SPEED))
        || (currentPongObjects.multiball.hitEvent && (currentPongObjects.multiball.currentVelocity.x > PAD_HIT_VERY_FAST_SOUND_SPEED || currentPongObjects.multiball.currentVelocity.x < -PAD_HIT_VERY_FAST_SOUND_SPEED)))
    {
        PlaySoundMulti(gameSounds.padHitVeryFastSounds);
    }
    else if ((currentPongObjects.ball.trailActive && currentPongObjects.ball.hitEvent) || (currentPongObjects.multiball.trailActive && currentPongObjects.multiball.hitEvent))
    {
        PlaySoundMulti(gameSounds.padHitFastSounds);
    }
    else if(currentPongObjects.ball.hitEvent || currentPongObjects.multiball.hitEvent)
    {
        PlaySoundMulti(gameSounds.padHitSounds);
    }

    if(currentPongObjects.ball.goalEvent || currentPongObjects.multiball.goalEvent)
    {
        PlaySoundMulti(gameSounds.goalSound);
    }

}

void MenuAudio(bool moveEvent, bool selectEvent)
{

    using namespace audio;

    if (moveEvent)   { PlaySoundMulti(gameSounds.menuMoveSound); }
    if (selectEvent) { PlaySoundMulti(gameSounds.menuSelectSound); }

    UpdateMusicStream(gameMusic.MenuMusic);

    if(audio::changeVolume)
    {

        audio::changeVolume = false;
        SetAllMusicVolume(gameplay::currentGameState, gameMusic);
        SetAllSoundVolume(gameplay::currentGameState, gameSounds);
        SaveStorageValue(SAVE_POSITION_SOUND_VOLUME, static_cast<int>(gameplay::currentGameState.soundVolume * SAVE_VOLUME_MULTIPLIER));
        SaveStorageValue(SAVE_POSITION_MUSIC_VOLUME, static_cast<int>(gameplay::currentGameState.musicVolume * SAVE_VOLUME_MULTIPLIER));

    }

}

void UnloadSoundsFromMemory(GameSounds& gameSounds)
{

    UnloadSound(gameSounds.padHitSounds);
    UnloadSound(gameSounds.padHitFastSounds);
    UnloadSound(gameSounds.padHitVeryFastSounds);
    UnloadSound(gameSounds.goalSound);
    UnloadSound(gameSounds.menuMoveSound);
    UnloadSound(gameSounds.menuSelectSound);

}

void UnloadMusicFromMemory(GameMusic& gameMusic)
{

    UnloadMusicStream(gameMusic.MatchMusic);
    UnloadMusicStream(gameMusic.MenuMusic);

}
