#include "credits.h"
#include "External Variables/extern_variables.h"

namespace scenes
{
    namespace credits
    {

        void update() 
        {

            UpdateMusicStream(audio::gameMusic.MenuMusic);

            if (gameplay::input.enter)
            {
                gameplay::currentGameState.nextGameMenu = GAME_MENU::MAIN_MENU;
            }
        }

        void draw() 
        {
            using namespace screen;

            DrawText(FormatText("Built using Raylib library"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Built using Raylib library"), 20) / 2), SCREEN_HEIGHT * 2 / 8, 20, BLACK);

            DrawText(FormatText("Music and sounds taken from Super Hexagon OST"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Music and sounds taken from Super Hexagon OST"), 20) / 2), SCREEN_HEIGHT * 3 / 8, 20, BLACK);

            DrawText(FormatText("Made by Adrian Sgro"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Made by Adrian Sgro"), 20) / 2), SCREEN_HEIGHT * 4 / 8, 20, BLACK);

            DrawText(FormatText("Press enter to get back to the main menu"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press enter to get back to the main menu"), 20) / 2), SCREEN_HEIGHT * 7 / 8, 20, BLACK);

            DrawText(FormatText("Version - 1.0"), SCREEN_WIDTH * 15 / 16 - (MeasureText(FormatText("Version - 1.0"), 20) / 2), SCREEN_HEIGHT * 15 / 16, 20, BLACK);

        }

        void init() 
        {

        }

        void deInit() 
        {

        }

    }
}