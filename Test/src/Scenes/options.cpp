#pragma once
#include "raylib.h"
#include "External Variables/extern_variables.h"

void optionScreenLogic();

namespace scenes
{
    namespace options
    {
        using namespace gameplay;

        void update() 
        {

			optionScreenLogic();

            MenuAudio(currentGameMenus.optionScreen.moveEvent, currentGameMenus.optionScreen.selectEvent);

        }

        void draw() 
        {

			using namespace screen;
			using namespace gameplay;

			if (currentGameMenus.optionScreen.settingMusicVolume)
			{
				DrawText(FormatText(MUSIC_VOLUME_TEXT), SCREEN_WIDTH_HALF - (MeasureText(MUSIC_VOLUME_TEXT, 50) / 2), screen::SCREEN_HEIGHT_HALF, 50, BLACK);
				Rectangle recOverlay{ SCREEN_WIDTH_HALF - 325, screen::SCREEN_HEIGHT_QUARTER, 660, 100 };
				DrawRectangleLinesEx(recOverlay, 4, BLACK);
				int aux = 1;
				while (aux <= currentGameState.musicVolume * 10)
				{
					DrawRectangle(static_cast<int>(SCREEN_WIDTH_HALF - 350 + aux * (float)60), screen::SCREEN_HEIGHT_QUARTER + 25, 50, 50, BLACK);
					aux++;
				}
			}
			else if (currentGameMenus.optionScreen.settingSoundVolume)
			{
				DrawText(FormatText(SOUND_VOLUME_TEXT), SCREEN_WIDTH_HALF - (MeasureText(SOUND_VOLUME_TEXT, 50) / 2), screen::SCREEN_HEIGHT_HALF, 50, BLACK);
				Rectangle recOverlay{ SCREEN_WIDTH_HALF - 325, screen::SCREEN_HEIGHT_QUARTER, 660, 100 };
				DrawRectangleLinesEx(recOverlay, 4, BLACK);
				float aux = 1;
				while (aux <= currentGameState.soundVolume * 10)
				{
					DrawRectangle(static_cast<int>(SCREEN_WIDTH_HALF - 350 + aux * (float)60), screen::SCREEN_HEIGHT_QUARTER + 25, 50, 50, BLACK);
					aux++;
				}
			}
			else if (currentGameMenus.optionScreen.settingNewKeys)
			{

				switch (currentGameMenus.optionScreen.currentKeyChangePosition)
				{
				case KEY_CHANGE_POSITION::LEFT_UP:
					DrawText(FormatText("Press the left pad new up key"), SCREEN_WIDTH_HALF - (MeasureText("Press the left pad new up key", 50) / 2), SCREEN_HEIGHT_QUARTER, 50, BLACK);
					break;
				case KEY_CHANGE_POSITION::LEFT_DOWN:
					DrawText(FormatText("Press the left pad new down key"), SCREEN_WIDTH_HALF - (MeasureText("Press the left pad new down key", 50) / 2), SCREEN_HEIGHT_QUARTER, 50, BLACK);
					break;
				case KEY_CHANGE_POSITION::LEFT_LEFT:
					DrawText(FormatText("Press the left pad new left key"), SCREEN_WIDTH_HALF - (MeasureText("Press the left pad new left key", 50) / 2), SCREEN_HEIGHT_QUARTER, 50, BLACK);
					break;
				case KEY_CHANGE_POSITION::LEFT_RIGHT:
					DrawText(FormatText("Press the left pad new right key"), SCREEN_WIDTH_HALF - (MeasureText("Press the left pad new right key", 50) / 2), SCREEN_HEIGHT_QUARTER, 50, BLACK);
					break;
				case KEY_CHANGE_POSITION::RIGHT_UP:
					DrawText(FormatText("Press the right pad new up key"), SCREEN_WIDTH_HALF - (MeasureText("Press the right pad new up key", 50) / 2), SCREEN_HEIGHT_QUARTER, 50, BLACK);
					break;
				case KEY_CHANGE_POSITION::RIGHT_DOWN:
					DrawText(FormatText("Press the right pad new down key"), SCREEN_WIDTH_HALF - (MeasureText("Press the right pad new down key", 50) / 2), SCREEN_HEIGHT_QUARTER, 50, BLACK);
					break;
				case KEY_CHANGE_POSITION::RIGHT_LEFT:
					DrawText(FormatText("Press the right pad new left key"), SCREEN_WIDTH_HALF - (MeasureText("Press the right pad new left key", 50) / 2), SCREEN_HEIGHT_QUARTER, 50, BLACK);
					break;
				case KEY_CHANGE_POSITION::RIGHT_RIGHT:
					DrawText(FormatText("Press the right pad new right key"), SCREEN_WIDTH_HALF - (MeasureText("Press the right pad new right key", 50) / 2), SCREEN_HEIGHT_QUARTER, 50, BLACK);
					break;
				default:
					break;
				}

				switch (gameplay::currentKeycodes.down_W)
				{

				case KEY_UP:
					DrawText(FormatText("Left pad - Up: Up Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Up: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 10 / 16, 20, BLACK);
					break;

				case KEY_DOWN:
					DrawText(FormatText("Left pad - Up: Down Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Up: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 10 / 16, 20, BLACK);
					break;

				case KEY_LEFT:
					DrawText(FormatText("Left pad - Up: Left Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Up: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 10 / 16, 20, BLACK);
					break;

				case KEY_RIGHT:
					DrawText(FormatText("Left pad - Up: Right Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Up: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 10 / 16, 20, BLACK);
					break;

				default:
					DrawText(FormatText("Left pad - Up: %c", (char)gameplay::currentKeycodes.down_W), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Up: %c", (char)gameplay::currentKeycodes.down_W), 20) / 2), SCREEN_HEIGHT * 10 / 16, 20, BLACK);
					break;

				}

				switch (gameplay::currentKeycodes.down_S)
				{

				case KEY_UP:
					DrawText(FormatText("Left pad - Down: Up Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Down: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 11 / 16, 20, BLACK);
					break;

				case KEY_DOWN:
					DrawText(FormatText("Left pad - Down: Down Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Down: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 11 / 16, 20, BLACK);
					break;

				case KEY_LEFT:
					DrawText(FormatText("Left pad - Down: Left Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Down: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 11 / 16, 20, BLACK);
					break;

				case KEY_RIGHT:
					DrawText(FormatText("Left pad - Down: Right Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Down: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 11 / 16, 20, BLACK);
					break;

				default:
					DrawText(FormatText("Left pad - Down: %c", (char)gameplay::currentKeycodes.down_S), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Down: %c", (char)gameplay::currentKeycodes.down_S), 20) / 2), SCREEN_HEIGHT * 11 / 16, 20, BLACK);
					break;

				}

				switch (gameplay::currentKeycodes.pressed_A)
				{

				case KEY_UP:
					DrawText(FormatText("Left pad - Left: Up Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Left: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);
					break;

				case KEY_DOWN:
					DrawText(FormatText("Left pad - Left: Down Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Left: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);
					break;

				case KEY_LEFT:
					DrawText(FormatText("Left pad - Left: Left Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Left: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);
					break;

				case KEY_RIGHT:
					DrawText(FormatText("Left pad - Left: Right Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Left: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);
					break;

				default:
					DrawText(FormatText("Left pad - Left: %c", (char)gameplay::currentKeycodes.pressed_A), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Left: %c", (char)gameplay::currentKeycodes.pressed_A), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);
					break;

				}

				switch (gameplay::currentKeycodes.pressed_D)
				{

				case KEY_UP:
					DrawText(FormatText("Left pad - Right: Up Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Right: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
					break;

				case KEY_DOWN:
					DrawText(FormatText("Left pad - Right: Down Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Right: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
					break;

				case KEY_LEFT:
					DrawText(FormatText("Left pad - Right: Left Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Right: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
					break;

				case KEY_RIGHT:
					DrawText(FormatText("Left pad - Right: Right Arrow"), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Right: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
					break;

				default:
					DrawText(FormatText("Left pad - Right: %c", (char)gameplay::currentKeycodes.pressed_D), SCREEN_WIDTH_QUARTER - (MeasureText(FormatText("Left pad - Right: %c", (char)gameplay::currentKeycodes.pressed_D), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
					break;

				}

				switch (gameplay::currentKeycodes.down_UpArrow)
				{

				case KEY_UP:
					DrawText(FormatText("Right pad - Up: Up Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Up: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 10 / 16, 20, BLACK);
					break;

				case KEY_DOWN:
					DrawText(FormatText("Right pad - Up: Down Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Up: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 10 / 16, 20, BLACK);
					break;

				case KEY_LEFT:
					DrawText(FormatText("Right pad - Up: Left Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Up: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 10 / 16, 20, BLACK);
					break;

				case KEY_RIGHT:
					DrawText(FormatText("Right pad - Up: Right Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Up: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 10 / 16, 20, BLACK);
					break;

				default:
					DrawText(FormatText("Right pad - Up: %c", (char)gameplay::currentKeycodes.down_UpArrow), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Up: %c", (char)gameplay::currentKeycodes.down_UpArrow), 20) / 2), SCREEN_HEIGHT * 10 / 16, 20, BLACK);
					break;

				}

				switch (gameplay::currentKeycodes.down_DownArrow)
				{

				case KEY_UP:
					DrawText(FormatText("Right pad - Down: Up Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Down: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 11 / 16, 20, BLACK);
					break;

				case KEY_DOWN:
					DrawText(FormatText("Right pad - Down: Down Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Down: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 11 / 16, 20, BLACK);
					break;

				case KEY_LEFT:
					DrawText(FormatText("Right pad - Down: Left Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Down: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 11 / 16, 20, BLACK);
					break;

				case KEY_RIGHT:
					DrawText(FormatText("Right pad - Down: Right Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Down: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 11 / 16, 20, BLACK);
					break;

				default:
					DrawText(FormatText("Right pad - Down: %c", (char)gameplay::currentKeycodes.down_DownArrow), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Down: %c", (char)gameplay::currentKeycodes.down_DownArrow), 20) / 2), SCREEN_HEIGHT * 11 / 16, 20, BLACK);
					break;

				}

				switch (gameplay::currentKeycodes.pressed_LeftArrow)
				{

				case KEY_UP:
					DrawText(FormatText("Right pad - Left: Up Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Left: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);
					break;

				case KEY_DOWN:
					DrawText(FormatText("Right pad - Left: Down Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Left: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);
					break;

				case KEY_LEFT:
					DrawText(FormatText("Right pad - Left: Left Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Left: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);
					break;

				case KEY_RIGHT:
					DrawText(FormatText("Right pad - Left: Right Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Left: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);
					break;

				default:
					DrawText(FormatText("Right pad - Left: %c", (char)gameplay::currentKeycodes.pressed_LeftArrow), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Left: %c", (char)gameplay::currentKeycodes.pressed_LeftArrow), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);
					break;

				}

				switch (gameplay::currentKeycodes.pressed_RightArrow)
				{

				case KEY_UP:
					DrawText(FormatText("Right pad - Right: Up Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Right: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
					break;

				case KEY_DOWN:
					DrawText(FormatText("Right pad - Right: Down Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Right: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
					break;

				case KEY_LEFT:
					DrawText(FormatText("Right pad - Right: Left Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Right: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
					break;

				case KEY_RIGHT:
					DrawText(FormatText("Right pad - Right: Right Arrow"), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Right: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
					break;

				default:
					DrawText(FormatText("Right pad - Right: %c", (char)gameplay::currentKeycodes.pressed_RightArrow), SCREEN_WIDTH * 6 / 8 - (MeasureText(FormatText("Right pad - Right: %c", (char)gameplay::currentKeycodes.pressed_RightArrow), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
					break;

				}

			}
			else
			{

				Color selected;

				currentGameMenus.optionScreen.musicVolumeButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
				DrawText(FormatText(MUSIC_VOLUME_TEXT), SCREEN_WIDTH_HALF - (MeasureText(MUSIC_VOLUME_TEXT, 50) / 2), screen::SCREEN_HEIGHT * 1 / 6, 50, selected);

				currentGameMenus.optionScreen.soundVolumeButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
				DrawText(FormatText(SOUND_VOLUME_TEXT), SCREEN_WIDTH_HALF - (MeasureText(SOUND_VOLUME_TEXT, 50) / 2), screen::SCREEN_HEIGHT * 2 / 6, 50, selected);

				currentGameMenus.optionScreen.changeInputButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
				DrawText(FormatText(CHANGE_INPUT_KEYS_TEXT), SCREEN_WIDTH_HALF - (MeasureText(CHANGE_INPUT_KEYS_TEXT, 50) / 2), screen::SCREEN_HEIGHT * 3 / 6, 50, selected);

				currentGameMenus.optionScreen.backButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
				DrawText(FormatText(BACK_TEXT), SCREEN_WIDTH_HALF - (MeasureText(BACK_TEXT, 50) / 2), screen::SCREEN_HEIGHT * 4 / 6, 50, selected);
			}
        }

		void init()
		{

			gameplay::currentGameMenus.optionScreen.musicVolumeButton.selected = true;

		}

		void deInit()
		{

			gameplay::currentGameMenus.optionScreen.musicVolumeButton.selected = false;
			gameplay::currentGameMenus.optionScreen.soundVolumeButton.selected = false;
			gameplay::currentGameMenus.optionScreen.changeInputButton.selected = false;
			gameplay::currentGameMenus.optionScreen.backButton.selected = false;

		}

    }
}

void optionScreenLogic()
{

	using namespace gameplay;

	OptionScreen* optionScreen_Ptr = &gameplay::currentGameMenus.optionScreen;

	if (optionScreen_Ptr->moveEvent) { optionScreen_Ptr->moveEvent = false; }
	if (optionScreen_Ptr->selectEvent) { optionScreen_Ptr->selectEvent = false; }

	if (optionScreen_Ptr->settingMusicVolume)
	{
		changeVolume(optionScreen_Ptr->moveEvent, optionScreen_Ptr->selectEvent, optionScreen_Ptr->settingMusicVolume, currentGameState.musicVolume);
	}
	else if (optionScreen_Ptr->settingSoundVolume)
	{
		changeVolume(optionScreen_Ptr->moveEvent, optionScreen_Ptr->selectEvent, optionScreen_Ptr->settingSoundVolume, currentGameState.soundVolume);
	}
	else if (optionScreen_Ptr->settingNewKeys)
	{

		int aux = 0;

		if(IsKeyPressed(KEY_UP))
		{
			aux = KEY_UP;
		}
		else if (IsKeyPressed(KEY_DOWN))
		{
			aux = KEY_DOWN;
		}
		else if (IsKeyPressed(KEY_LEFT))
		{
			aux = KEY_LEFT;
		}
		else if (IsKeyPressed(KEY_RIGHT))
		{
			aux = KEY_RIGHT;
		}
		else
		{
			aux = GetKeyPressed();
			aux -= 32;
		}

		switch (optionScreen_Ptr->currentKeyChangePosition)
		{
		case KEY_CHANGE_POSITION::LEFT_UP:
			if (aux != -32)
			{
				optionScreen_Ptr->selectEvent = true;
				gameplay::currentKeycodes.down_W = aux;
				gameplay::currentKeycodes.pressed_W = aux;
				optionScreen_Ptr->currentKeyChangePosition = (KEY_CHANGE_POSITION)(static_cast<int>(optionScreen_Ptr->currentKeyChangePosition) + 1);
			}
			break;
		case KEY_CHANGE_POSITION::LEFT_DOWN:
			if (aux != -32)
			{
				optionScreen_Ptr->selectEvent = true;
				gameplay::currentKeycodes.down_S = aux;
				gameplay::currentKeycodes.pressed_S = aux;
				optionScreen_Ptr->currentKeyChangePosition = (KEY_CHANGE_POSITION)(static_cast<int>(optionScreen_Ptr->currentKeyChangePosition) + 1);
			}
			break;
		case KEY_CHANGE_POSITION::LEFT_LEFT:
			if (aux != -32)
			{
				optionScreen_Ptr->selectEvent = true;
				gameplay::currentKeycodes.pressed_A = aux;
				optionScreen_Ptr->currentKeyChangePosition = (KEY_CHANGE_POSITION)(static_cast<int>(optionScreen_Ptr->currentKeyChangePosition) + 1);
			}
			break;
		case KEY_CHANGE_POSITION::LEFT_RIGHT:
			if (aux != -32)
			{
				optionScreen_Ptr->selectEvent = true;
				gameplay::currentKeycodes.pressed_D = aux;
				optionScreen_Ptr->currentKeyChangePosition = (KEY_CHANGE_POSITION)(static_cast<int>(optionScreen_Ptr->currentKeyChangePosition) + 1);
			}
			break;
		case KEY_CHANGE_POSITION::RIGHT_UP:
			if (aux != -32)
			{
				optionScreen_Ptr->selectEvent = true;
				gameplay::currentKeycodes.down_UpArrow = aux;
				gameplay::currentKeycodes.pressed_UpArrow = aux;
				optionScreen_Ptr->currentKeyChangePosition = (KEY_CHANGE_POSITION)(static_cast<int>(optionScreen_Ptr->currentKeyChangePosition) + 1);
			}
			break;
		case KEY_CHANGE_POSITION::RIGHT_DOWN:
			if (aux != -32)
			{
				optionScreen_Ptr->selectEvent = true;
				gameplay::currentKeycodes.down_DownArrow = aux;
				gameplay::currentKeycodes.pressed_DownArrow = aux;
				optionScreen_Ptr->currentKeyChangePosition = (KEY_CHANGE_POSITION)(static_cast<int>(optionScreen_Ptr->currentKeyChangePosition) + 1);
			}
			break;
		case KEY_CHANGE_POSITION::RIGHT_LEFT:
			if (aux != -32)
			{
				optionScreen_Ptr->selectEvent = true;
				gameplay::currentKeycodes.pressed_LeftArrow = aux;
				optionScreen_Ptr->currentKeyChangePosition = (KEY_CHANGE_POSITION)(static_cast<int>(optionScreen_Ptr->currentKeyChangePosition) + 1);
			}
			break;
		case KEY_CHANGE_POSITION::RIGHT_RIGHT:
			if (aux != -32)
			{
				optionScreen_Ptr->selectEvent = true;
				gameplay::currentKeycodes.pressed_RightArrow = aux;
				optionScreen_Ptr->currentKeyChangePosition = (KEY_CHANGE_POSITION)(static_cast<int>(optionScreen_Ptr->currentKeyChangePosition) + 1);
			}
			break;
		default:
			break;
		}

		if (optionScreen_Ptr->currentKeyChangePosition >= KEY_CHANGE_POSITION::SIZE)
		{
			optionScreen_Ptr->currentKeyChangePosition = (KEY_CHANGE_POSITION)0;
			optionScreen_Ptr->settingNewKeys = false;
		}

	}
	else
	{
		if (input.pressed_W || input.pressed_UpArrow)
		{

			optionScreen_Ptr->moveEvent = true;
			if (optionScreen_Ptr->musicVolumeButton.selected)
			{
				optionScreen_Ptr->musicVolumeButton.selected = false;
				optionScreen_Ptr->backButton.selected = true;
			}
			else if (optionScreen_Ptr->soundVolumeButton.selected)
			{
				optionScreen_Ptr->soundVolumeButton.selected = false;
				optionScreen_Ptr->musicVolumeButton.selected = true;
			}
			else if (optionScreen_Ptr->changeInputButton.selected)
			{
				optionScreen_Ptr->changeInputButton.selected = false;
				optionScreen_Ptr->soundVolumeButton.selected = true;
			}
			else //if(optionScreenPtr->backButton.selected)
			{
				optionScreen_Ptr->backButton.selected = false;
				optionScreen_Ptr->changeInputButton.selected = true;
			}

		}
		else if (input.pressed_S || input.pressed_DownArrow)
		{

			optionScreen_Ptr->moveEvent = true;
			if (optionScreen_Ptr->musicVolumeButton.selected)
			{
				optionScreen_Ptr->musicVolumeButton.selected = false;
				optionScreen_Ptr->soundVolumeButton.selected = true;
			}
			else if (optionScreen_Ptr->soundVolumeButton.selected)
			{
				optionScreen_Ptr->soundVolumeButton.selected = false;
				optionScreen_Ptr->changeInputButton.selected = true;
			}
			else if (optionScreen_Ptr->changeInputButton.selected)
			{
				optionScreen_Ptr->changeInputButton.selected = false;
				optionScreen_Ptr->backButton.selected = true;
			}
			else //if(optionScreenPtr->backButton.selected)
			{
				optionScreen_Ptr->backButton.selected = false;
				optionScreen_Ptr->musicVolumeButton.selected = true;
			}

		}

		if (input.enter)
		{
			optionScreen_Ptr->selectEvent = true;
			if (optionScreen_Ptr->musicVolumeButton.selected)
			{
				optionScreen_Ptr->settingMusicVolume = true;
			}
			else if (optionScreen_Ptr->soundVolumeButton.selected)
			{
				optionScreen_Ptr->settingSoundVolume = true;
			}
			else if (optionScreen_Ptr->changeInputButton.selected)
			{
				optionScreen_Ptr->settingNewKeys = true;
			}
			else //if(optionScreenPtr->backButton.selected)
			{
				currentGameState.nextGameMenu = GAME_MENU::MAIN_MENU;
			}
		}
	}


}