#include "selection_screen.h"
#include "External Variables/extern_variables.h"

void selectionScreenLogic();

void buildDemo();

namespace scenes
{
    namespace selection_screen
    {

        void update() 
        {

            selectionScreenLogic();

            MenuAudio(gameplay::currentGameMenus.selectionScreen.moveEvent, gameplay::currentGameMenus.selectionScreen.selectEvent);

        }

        void draw() 
        {

			using namespace screen;
			using namespace gameplay;

			SelectionScreen* selectionScreen_Ptr = &currentGameMenus.selectionScreen;

			if(!selectionScreen_Ptr->matchConfig)
			{

				if (currentPongObjects.padLeft.control == CONTROL::PLAYER)
				{
					DrawText(FormatText("PLAYER"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("PLAYER"), 20) / 2), SCREEN_HEIGHT_HALF - 20, 20, currentPongObjects.padLeft.color);
				}
				else if (currentPongObjects.padLeft.control == CONTROL::AI_EASY)
				{
					DrawText(FormatText("AI EASY"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("AI EASY"), 20) / 2), SCREEN_HEIGHT_HALF - 20, 20, currentPongObjects.padLeft.color);
				}
				else if (currentPongObjects.padLeft.control == CONTROL::AI_NORMAL)
				{
					DrawText(FormatText("AI NORMAL"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("AI NORMAL"), 20) / 2), SCREEN_HEIGHT_HALF - 20, 20, currentPongObjects.padLeft.color);
				}
				else
				{
					DrawText(FormatText("AI HARD"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("AI HARD"), 20) / 2), SCREEN_HEIGHT_HALF - 20, 20, currentPongObjects.padLeft.color);
				}

				if (currentPongObjects.padRight.control == CONTROL::PLAYER)
				{
					DrawText(FormatText("PLAYER"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("PLAYER"), 20) / 2), SCREEN_HEIGHT_HALF - 20, 20, currentPongObjects.padRight.color);
				}
				else if (currentPongObjects.padRight.control == CONTROL::AI_EASY)
				{
					DrawText(FormatText("AI EASY"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("AI EASY"), 20) / 2), SCREEN_HEIGHT_HALF - 20, 20, currentPongObjects.padRight.color);
				}
				else if (currentPongObjects.padRight.control == CONTROL::AI_NORMAL)
				{
					DrawText(FormatText("AI NORMAL"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("AI NORMAL"), 20) / 2), SCREEN_HEIGHT_HALF - 20, 20, currentPongObjects.padRight.color);
				}
				else
				{
					DrawText(FormatText("AI HARD"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("AI HARD"), 20) / 2), SCREEN_HEIGHT_HALF - 20, 20, currentPongObjects.padRight.color);
				}

				DrawText(FormatText(SELECTION_SCREEN_VS), SCREEN_WIDTH_HALF - (MeasureText(FormatText(SELECTION_SCREEN_VS), 100) / 2), SCREEN_HEIGHT_HALF - 50, 100, BLACK);

				DrawText(FormatText("Press C to change match configs"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press C to change match configs"), 20) / 2), SCREEN_HEIGHT_EIGHT, 20, BLACK);

				DrawText(FormatText("Press Enter to start playing"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press Enter to start playing"), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);

				DrawText(FormatText("Press left/right keys to change control type"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press left/right keys to change control type"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);

				DrawText(FormatText("Press up/down keys to change pad color"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press up/down keys to change pad color"), 20) / 2), SCREEN_HEIGHT * 14 / 16, 20, BLACK);

				DrawText(FormatText(SELECTION_SCREEN_LEFT_SIDE_TEXT), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText(SELECTION_SCREEN_LEFT_SIDE_TEXT), 20) / 2), SCREEN_HEIGHT_HALF + 20, 20, currentPongObjects.padLeft.color);

				DrawRectangle(SCREEN_WIDTH_QUARTER, SCREEN_HEIGHT_HALF - 250 / 2, 50, 250, currentPongObjects.padLeft.color);

				DrawText(FormatText(SELECTION_SCREEN_RIGHT_SIDE_TEXT), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText(SELECTION_SCREEN_RIGHT_SIDE_TEXT), 20) / 2), SCREEN_HEIGHT_HALF + 20, 20, currentPongObjects.padRight.color);

				DrawRectangle(SCREEN_WIDTH * 6 / 8 - 50, SCREEN_HEIGHT_HALF - 250 / 2, 50, 250, currentPongObjects.padRight.color);

			}
			else
			{

				DrawRectangleRec(selectionScreen_Ptr->padDemo, BLUE);

				DrawCircle(static_cast<int>(selectionScreen_Ptr->ballDemo.pos.x), static_cast<int>(selectionScreen_Ptr->ballDemo.pos.y), static_cast<float>(selectionScreen_Ptr->ballDemo.radius), RED);

				DrawRectangleLines(screen::SCREEN_WIDTH * 11 / 16, screen::SCREEN_HEIGHT * 1 / 32, screen::SCREEN_WIDTH * 1 / 4, screen::SCREEN_HEIGHT * 3 / 4, BLACK);

				DrawText(FormatText("DEMO SCENE"), SCREEN_WIDTH * 11 / 16 - (MeasureText(FormatText("DEMO SCENE"), 20) / 2) + screen::SCREEN_WIDTH_EIGHT, SCREEN_HEIGHT * 23 / 32, 20, BLACK);

				Color selected;

				selectionScreen_Ptr->goalsPerRoundButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
				DrawText(FormatText("Goals per round: %01i", config::goalPerRound), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Goals per round: %01i", config::goalPerRound), 20) / 2), screen::SCREEN_HEIGHT * 5 / 16, 20, selected);

				selectionScreen_Ptr->padSpeedButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
				DrawText(FormatText("Pad Speed: %01i", config::padSpeed), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Pad Speed: %01i", config::padSpeed), 20) / 2), screen::SCREEN_HEIGHT * 3 / 8, 20, selected);

				selectionScreen_Ptr->padSizeButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
				DrawText(FormatText("Pad Size: %01i", config::padSize), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Pad Size: %01i", config::padSize), 20) / 2), screen::SCREEN_HEIGHT * 7 / 16, 20, selected);

				selectionScreen_Ptr->ballSizeButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
				DrawText(FormatText("Ball Size: %01i", config::ballSize), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Ball Size: %01i", config::ballSize), 20) / 2), screen::SCREEN_HEIGHT * 1 / 2, 20, selected);

				if(!config::superMode)
				{

					selectionScreen_Ptr->matchTypeButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
					DrawText(FormatText("Classic Pong"), SCREEN_WIDTH_HALF - (MeasureText("Classic Pong", 20) / 2), screen::SCREEN_HEIGHT_QUARTER, 20, selected);

					selected = LOCKED_SELECTION_COLOR;
					DrawText(FormatText("Hits for pads Power Ups: N/A"), SCREEN_WIDTH_HALF - (MeasureText("Hits for pads Power Ups: N/A", 20) / 2), screen::SCREEN_HEIGHT * 9 / 16, 20, selected);
					DrawText(FormatText("Time for random Power Ups: N/A"), SCREEN_WIDTH_HALF - (MeasureText("Time for random Power Ups: N/A", 20) / 2), screen::SCREEN_HEIGHT * 5 / 8, 20, selected);

				}
				else
				{

					selectionScreen_Ptr->matchTypeButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
					DrawText(FormatText("Super Pong"), SCREEN_WIDTH_HALF - (MeasureText("Super Pong", 20) / 2), screen::SCREEN_HEIGHT_QUARTER, 20, selected);

					selectionScreen_Ptr->hitsForPowerUpButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
					DrawText(FormatText("Hits for pads Power Ups: %01i", config::hitsForPowerUp), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Hits for Pads Power Ups: %01i", config::hitsForPowerUp), 20) / 2), screen::SCREEN_HEIGHT * 9 / 16, 20, selected);
					selectionScreen_Ptr->timeRandomPowerUpButton.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
					DrawText(FormatText("Time for random Power Ups: %01i", config::timeForRandomPowerUp), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Time for random Power Ups: %01i", config::timeForRandomPowerUp), 20) / 2), screen::SCREEN_HEIGHT * 5 / 8, 20, selected);

				}

				DrawText(FormatText("Press C to return to pads configs"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press C to return to pads configs"), 20) / 2), SCREEN_HEIGHT_EIGHT, 20, BLACK);

				DrawText(FormatText("Press Enter to start playing"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press Enter to start playing"), 20) / 2), SCREEN_HEIGHT * 12 / 16, 20, BLACK);

				DrawText(FormatText("Press left/right keys to change selected option configuration"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press left/right keys to change selected option configuration"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);

				DrawText(FormatText("Press up/down keys to change selected option"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press up/down keys to change selected option"), 20) / 2), SCREEN_HEIGHT * 14 / 16, 20, BLACK);

			}

        }

        void init()
        {

			gameplay::currentGameMenus.selectionScreen.matchTypeButton.selected = true;

			buildDemo();

        }

        void deInit()
        {

			StopMusicStream(audio::gameMusic.MenuMusic);
			gameplay::currentGameMenus.selectionScreen.matchConfig = false;
			gameplay::currentGameMenus.selectionScreen.ballSizeButton.selected = false;
			gameplay::currentGameMenus.selectionScreen.goalsPerRoundButton.selected = false;
			gameplay::currentGameMenus.selectionScreen.hitsForPowerUpButton.selected = false;
			gameplay::currentGameMenus.selectionScreen.matchTypeButton.selected = false;
			gameplay::currentGameMenus.selectionScreen.padSizeButton.selected = false;
			gameplay::currentGameMenus.selectionScreen.padSpeedButton.selected = false;
			gameplay::currentGameMenus.selectionScreen.timeRandomPowerUpButton.selected = false;

        }

    }
}

void buildDemo()
{
	gameplay::currentGameMenus.selectionScreen.padDemo.height = static_cast<float>(config::padSize);
	gameplay::currentGameMenus.selectionScreen.padDemo.width = static_cast<float>(config::padSize) / PAD_SIZE_RELATION;
	gameplay::currentGameMenus.selectionScreen.padDemo.y = screen::SCREEN_HEIGHT * 3 / 8 - static_cast<float>(config::padSize) / 2;
	gameplay::currentGameMenus.selectionScreen.padDemo.x = screen::SCREEN_WIDTH * 6 / 8;

	gameplay::currentGameMenus.selectionScreen.ballDemo.radius = config::ballSize;
	gameplay::currentGameMenus.selectionScreen.ballDemo.pos.y = screen::SCREEN_HEIGHT * 3 / 8;
	gameplay::currentGameMenus.selectionScreen.ballDemo.pos.x = screen::SCREEN_WIDTH * 13 / 16;

}

void updateDemo()
{

	gameplay::currentGameMenus.selectionScreen.padDemo.height = static_cast<float>(config::padSize);
	gameplay::currentGameMenus.selectionScreen.padDemo.width = static_cast<float>(config::padSize) / PAD_SIZE_RELATION;
	gameplay::currentGameMenus.selectionScreen.ballDemo.radius = config::ballSize;

	if(gameplay::currentGameMenus.selectionScreen.padDemo.y + gameplay::currentGameMenus.selectionScreen.padDemo.height < screen::SCREEN_HEIGHT * 5 / 16)
	{
		gameplay::currentGameMenus.selectionScreen.padDemoDirectionDown = true;
	}
	else if(gameplay::currentGameMenus.selectionScreen.padDemo.y > screen::SCREEN_HEIGHT * 7 / 16)
	{
		gameplay::currentGameMenus.selectionScreen.padDemoDirectionDown = false;
	}

	if(gameplay::currentGameMenus.selectionScreen.padDemoDirectionDown)
	{
		gameplay::currentGameMenus.selectionScreen.padDemo.y += config::padSpeed;
	}
	else
	{
		gameplay::currentGameMenus.selectionScreen.padDemo.y -= config::padSpeed;
	}

}

void selectionScreenLogic()
{

	using namespace gameplay;

	SelectionScreen* selectionScreen_Ptr = &currentGameMenus.selectionScreen;

	if (selectionScreen_Ptr->moveEvent) { selectionScreen_Ptr->moveEvent = false; }
	if (selectionScreen_Ptr->selectEvent) { selectionScreen_Ptr->selectEvent = false; }

	if (input.enter)
	{

		gameplay::currentGameState.currentLeftScore = 0;
		gameplay::currentGameState.currentRightScore = 0;
		gameplay::currentGameState.nextGameMenu = GAME_MENU::IN_MATCH;
		selectionScreen_Ptr->selectEvent = true;

	}

	if (input.change)
	{
		selectionScreen_Ptr->matchConfig = !selectionScreen_Ptr->matchConfig;
		selectionScreen_Ptr->selectEvent = true;
	}

	if (!selectionScreen_Ptr->matchConfig)
	{

		if (input.pressed_W)
		{

			if (selectionScreen_Ptr->leftPadPosition < PAD_COLOR_SIZE - 1)
			{

				selectionScreen_Ptr->leftPadPosition++;
				currentPongObjects.padLeft.color = padColorsArray[selectionScreen_Ptr->leftPadPosition];
				selectionScreen_Ptr->moveEvent = true;

			}

		}
		else if (input.pressed_S)
		{

			if (selectionScreen_Ptr->leftPadPosition > 0)
			{

				selectionScreen_Ptr->leftPadPosition--;
				currentPongObjects.padLeft.color = padColorsArray[selectionScreen_Ptr->leftPadPosition];
				selectionScreen_Ptr->moveEvent = true;

			}

		}

		if (input.pressed_UpArrow)
		{

			if (selectionScreen_Ptr->rightPadPosition < PAD_COLOR_SIZE - 1)
			{

				selectionScreen_Ptr->rightPadPosition++;
				currentPongObjects.padRight.color = padColorsArray[selectionScreen_Ptr->rightPadPosition];
				selectionScreen_Ptr->moveEvent = true;

			}

		}
		else if (input.pressed_DownArrow)
		{

			if (selectionScreen_Ptr->rightPadPosition > 0)
			{

				selectionScreen_Ptr->rightPadPosition--;
				gameplay::currentPongObjects.padRight.color = padColorsArray[selectionScreen_Ptr->rightPadPosition];
				selectionScreen_Ptr->moveEvent = true;

			}

		}

		if (input.pressed_A && static_cast<int>(gameplay::currentPongObjects.padLeft.control) > 0)
		{
			int aux = static_cast<int>(gameplay::currentPongObjects.padLeft.control);
			aux--;
			gameplay::currentPongObjects.padLeft.control = (CONTROL)aux;
			selectionScreen_Ptr->moveEvent = true;
		}
		else if (input.pressed_D && static_cast<int>(gameplay::currentPongObjects.padLeft.control) < static_cast<int>(CONTROL::SIZE) - 1)
		{
			int aux = static_cast<int>(gameplay::currentPongObjects.padLeft.control);
			aux++;
			gameplay::currentPongObjects.padLeft.control = (CONTROL)aux;
			selectionScreen_Ptr->moveEvent = true;
		}

		if (input.pressed_LeftArrow && static_cast<int>(gameplay::currentPongObjects.padRight.control) > 0)
		{
			int aux = static_cast<int>(gameplay::currentPongObjects.padRight.control);
			aux--;
			gameplay::currentPongObjects.padRight.control = (CONTROL)aux;
			selectionScreen_Ptr->moveEvent = true;
		}
		else if (input.pressed_RightArrow && static_cast<int>(gameplay::currentPongObjects.padRight.control) < static_cast<int>(CONTROL::SIZE) - 1)
		{
			int aux = static_cast<int>(gameplay::currentPongObjects.padRight.control);
			aux++;
			gameplay::currentPongObjects.padRight.control = (CONTROL)aux;
			selectionScreen_Ptr->moveEvent = true;
		}

	}
	else
	{

		updateDemo();

		if (input.pressed_W || input.pressed_UpArrow)
		{
			selectionScreen_Ptr->moveEvent = true;
			if(selectionScreen_Ptr->matchTypeButton.selected)
			{
				selectionScreen_Ptr->matchTypeButton.selected = false;
				if(config::superMode)
				{
					selectionScreen_Ptr->timeRandomPowerUpButton.selected = true;
				}
				else
				{
					selectionScreen_Ptr->ballSizeButton.selected = true;
				}
			}
			else if (selectionScreen_Ptr->goalsPerRoundButton.selected)
			{
				selectionScreen_Ptr->goalsPerRoundButton.selected = false;
				selectionScreen_Ptr->matchTypeButton.selected = true;
			}
			else if (selectionScreen_Ptr->padSpeedButton.selected)
			{
				selectionScreen_Ptr->padSpeedButton.selected = false;
				selectionScreen_Ptr->goalsPerRoundButton.selected = true;
			}
			else if (selectionScreen_Ptr->padSizeButton.selected)
			{
				selectionScreen_Ptr->padSizeButton.selected = false;
				selectionScreen_Ptr->padSpeedButton.selected = true;
			}
			else if (selectionScreen_Ptr->ballSizeButton.selected)
			{
				selectionScreen_Ptr->ballSizeButton.selected = false;
				selectionScreen_Ptr->padSizeButton.selected = true;
			}
			else if (selectionScreen_Ptr->hitsForPowerUpButton.selected)
			{
				selectionScreen_Ptr->hitsForPowerUpButton.selected = false;
				selectionScreen_Ptr->ballSizeButton.selected = true;
			}
			else if (selectionScreen_Ptr->timeRandomPowerUpButton.selected)
			{
				selectionScreen_Ptr->timeRandomPowerUpButton.selected = false;
				selectionScreen_Ptr->hitsForPowerUpButton.selected = true;
			}
		}
		else if (input.pressed_S || input.pressed_DownArrow)
		{
			selectionScreen_Ptr->moveEvent = true;
			if (selectionScreen_Ptr->matchTypeButton.selected)
			{
				selectionScreen_Ptr->matchTypeButton.selected = false;
				selectionScreen_Ptr->goalsPerRoundButton.selected = true;
			}
			else if (selectionScreen_Ptr->goalsPerRoundButton.selected)
			{
				selectionScreen_Ptr->goalsPerRoundButton.selected = false;
				selectionScreen_Ptr->padSpeedButton.selected = true;
			}
			else if (selectionScreen_Ptr->padSpeedButton.selected)
			{
				selectionScreen_Ptr->padSpeedButton.selected = false;
				selectionScreen_Ptr->padSizeButton.selected = true;
			}
			else if (selectionScreen_Ptr->padSizeButton.selected)
			{
				selectionScreen_Ptr->padSizeButton.selected = false;
				selectionScreen_Ptr->ballSizeButton.selected = true;
			}
			else if (selectionScreen_Ptr->ballSizeButton.selected)
			{
				selectionScreen_Ptr->ballSizeButton.selected = false;
				if (config::superMode)
				{
					selectionScreen_Ptr->hitsForPowerUpButton.selected = true;
				}
				else
				{
					selectionScreen_Ptr->matchTypeButton.selected = true;
				}
			}
			else if (selectionScreen_Ptr->hitsForPowerUpButton.selected)
			{
				selectionScreen_Ptr->hitsForPowerUpButton.selected = false;
				selectionScreen_Ptr->timeRandomPowerUpButton.selected = true;
			}
			else if (selectionScreen_Ptr->timeRandomPowerUpButton.selected)
			{
				selectionScreen_Ptr->timeRandomPowerUpButton.selected = false;
				selectionScreen_Ptr->matchTypeButton.selected = true;
			}
		}
		else if (input.pressed_A || input.pressed_LeftArrow)
		{

			if (selectionScreen_Ptr->matchTypeButton.selected)
			{
				config::superMode = !config::superMode;
				selectionScreen_Ptr->moveEvent = true;
			}
			else if (selectionScreen_Ptr->goalsPerRoundButton.selected)
			{

				if(config::goalPerRound > GOAL_MIN_PER_MATCH)
				{
					selectionScreen_Ptr->moveEvent = true;
					config::goalPerRound--;
				}

			}
			else if (selectionScreen_Ptr->padSpeedButton.selected)
			{
				if(config::padSpeed > PAD_MIN_SPEED)
				{
					config::padSpeed--;
					selectionScreen_Ptr->moveEvent = true;
				}
			}
			else if (selectionScreen_Ptr->padSizeButton.selected)
			{
				if (config::padSize > PAD_MIN_HEIGHT)
				{
					config::padSize -= 10;
					selectionScreen_Ptr->moveEvent = true;
				}
			}
			else if (selectionScreen_Ptr->ballSizeButton.selected)
			{
				if (config::ballSize > BALL_MIN_SIZE)
				{
					config::ballSize--;
					selectionScreen_Ptr->moveEvent = true;
				}
			}
			else if (selectionScreen_Ptr->hitsForPowerUpButton.selected)
			{
				if (config::hitsForPowerUp > POWERUP_MIN_HITS)
				{
					config::hitsForPowerUp--;
					selectionScreen_Ptr->moveEvent = true;
				}
			}
			else if (selectionScreen_Ptr->timeRandomPowerUpButton.selected)
			{
				if (config::timeForRandomPowerUp > POWERUP_MIN_RANDOM_TIME)
				{
					config::timeForRandomPowerUp--;
					selectionScreen_Ptr->moveEvent = true;
				}
			}
		}
		else if (input.pressed_D || input.pressed_RightArrow)
		{

			if (selectionScreen_Ptr->matchTypeButton.selected)
			{
				config::superMode = !config::superMode;
				selectionScreen_Ptr->moveEvent = true;
			}
			else if (selectionScreen_Ptr->goalsPerRoundButton.selected)
			{

				if (config::goalPerRound < GOAL_MAX_PER_MATCH)
				{
					selectionScreen_Ptr->moveEvent = true;
					config::goalPerRound++;
				}

			}
			else if (selectionScreen_Ptr->padSpeedButton.selected)
			{
				if (config::padSpeed < PAD_MAX_SPEED)
				{
					config::padSpeed++;
					selectionScreen_Ptr->moveEvent = true;
				}
			}
			else if (selectionScreen_Ptr->padSizeButton.selected)
			{
				if (config::padSize < PAD_MAX_HEIGHT)
				{
					config::padSize += 10;
					selectionScreen_Ptr->moveEvent = true;
				}
			}
			else if (selectionScreen_Ptr->ballSizeButton.selected)
			{
				if (config::ballSize < BALL_MAX_SIZE)
				{
					config::ballSize++;
					selectionScreen_Ptr->moveEvent = true;
				}
			}
			else if (selectionScreen_Ptr->hitsForPowerUpButton.selected)
			{
				if (config::hitsForPowerUp < POWERUP_MAX_HITS)
				{
					config::hitsForPowerUp++;
					selectionScreen_Ptr->moveEvent = true;
				}
			}
			else if (selectionScreen_Ptr->timeRandomPowerUpButton.selected)
			{
				if (config::timeForRandomPowerUp < POWERUP_MAX_RANDOM_TIME)
				{
					config::timeForRandomPowerUp++;
					selectionScreen_Ptr->moveEvent = true;
				}
			}
		}

	}

}