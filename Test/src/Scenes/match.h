#ifndef MATCH_H
#define MATCH_H

#include "raylib.h"
#include "Gameplay/game_logic.h"
#include "Collision/collision.h"
#include "Gameplay/initialization.h"

namespace scenes
{
    namespace match
    {

        void update();

        void draw();

        void init();

        void deInit();

    }
}

#endif MATCH_H