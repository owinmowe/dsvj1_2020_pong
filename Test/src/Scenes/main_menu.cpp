#include "main_menu.h"
#include "External Variables/extern_variables.h"

void mainMenuLogic();

namespace scenes 
{
	namespace main_menu 
	{

		void update()
		{

			mainMenuLogic();

			MenuAudio(gameplay::currentGameMenus.mainMenu.moveEvent, gameplay::currentGameMenus.mainMenu.selectEvent);

		}

        void draw()
        {

			using namespace screen;

			using namespace gameplay;

            Color selected;

			if(!gameplay::currentGameMenus.mainMenu.titleDrawn)
			{

				gameplay::currentGameMenus.mainMenu.titleFrames++;

				DrawText(TextSubtext(MAIN_MENU_TITLE, 0, gameplay::currentGameMenus.mainMenu.titleFrames / MAIN_MENU_TITLE_SPEED), SCREEN_WIDTH_HALF - (MeasureText(FormatText(MAIN_MENU_TITLE), 100) / 2), screen::SCREEN_HEIGHT_EIGHT, 100, BLACK);

				if(gameplay::currentGameMenus.mainMenu.titleFrames > gameplay::currentGameMenus.mainMenu.titleFrames * MAIN_MENU_TITLE_SPEED)
				{

					gameplay::currentGameMenus.mainMenu.titleFrames = 0;

					gameplay::currentGameMenus.mainMenu.titleDrawn = true;

				}

			}
			else
			{

				DrawText(MAIN_MENU_TITLE, SCREEN_WIDTH_HALF - (MeasureText(FormatText(MAIN_MENU_TITLE), 100) / 2), screen::SCREEN_HEIGHT_EIGHT, 100, BLACK);

			}

			currentGameMenus.mainMenu.play.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
            DrawText(FormatText(MAIN_MENU_PLAY_TEXT), SCREEN_WIDTH_HALF - (MeasureText(FormatText(MAIN_MENU_PLAY_TEXT), 50) / 2), screen::SCREEN_HEIGHT * 2 / 6, 50, selected);

			currentGameMenus.mainMenu.options.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
            DrawText(FormatText(MAIN_MENU_OPTIONS_TEXT), SCREEN_WIDTH_HALF - (MeasureText(FormatText(MAIN_MENU_OPTIONS_TEXT), 50) / 2), screen::SCREEN_HEIGHT * 3 / 6, 50, selected);

			currentGameMenus.mainMenu.credits.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
            DrawText(FormatText(MAIN_MENU_CREDITS_TEXT), SCREEN_WIDTH_HALF - (MeasureText(FormatText(MAIN_MENU_CREDITS_TEXT), 50) / 2), screen::SCREEN_HEIGHT * 4 / 6, 50, selected);

			currentGameMenus.mainMenu.exit.selected ? selected = SELECTED_COLOR : selected = UNSELECTED_COLOR;
            DrawText(FormatText(MAIN_MENU_EXIT_TEXT), SCREEN_WIDTH_HALF - (MeasureText(FormatText(MAIN_MENU_EXIT_TEXT), 50) / 2), screen::SCREEN_HEIGHT * 5 / 6, 50, selected);

			DrawText(FormatText("Version - 1.0"), SCREEN_WIDTH * 15 / 16 - (MeasureText(FormatText("Version - 1.0"), 20) / 2), SCREEN_HEIGHT * 15 / 16, 20, BLACK);

        }

		void init()
		{

			gameplay::currentGameMenus.mainMenu.titleFrames = 0;

			gameplay::currentGameMenus.mainMenu.titleDrawn = false;

			gameplay::currentGameMenus.mainMenu.play.selected = true;

			PlayMusicStream(audio::gameMusic.MenuMusic);

		}

		void deInit()
		{

			gameplay::currentGameMenus.mainMenu.play.selected = false;

			gameplay::currentGameMenus.mainMenu.options.selected = false;

			gameplay::currentGameMenus.mainMenu.credits.selected = false;

			gameplay::currentGameMenus.mainMenu.exit.selected = false;

		}

	}
}

void mainMenuLogic()
{

	MainMenu* mainMenu_Ptr = &gameplay::currentGameMenus.mainMenu;

	if (mainMenu_Ptr->moveEvent) { mainMenu_Ptr->moveEvent = false; }
	if (mainMenu_Ptr->selectEvent) { mainMenu_Ptr->selectEvent = false; }

	if (gameplay::input.pressed_DownArrow || gameplay::input.pressed_S)
	{
		mainMenu_Ptr->moveEvent = true;
		if (mainMenu_Ptr->play.selected)
		{
			mainMenu_Ptr->play.selected = false;
			mainMenu_Ptr->options.selected = true;
		}
		else if (mainMenu_Ptr->options.selected)
		{
			mainMenu_Ptr->options.selected = false;
			mainMenu_Ptr->credits.selected = true;
		}
		else if (mainMenu_Ptr->credits.selected)
		{
			mainMenu_Ptr->credits.selected = false;
			mainMenu_Ptr->exit.selected = true;
		}
		else //if(mainMenu_Ptr->exit.selected)
		{
			mainMenu_Ptr->exit.selected = false;
			mainMenu_Ptr->play.selected = true;
		}
	}
	else if (gameplay::input.pressed_UpArrow || gameplay::input.pressed_W)
	{
		mainMenu_Ptr->moveEvent = true;
		if (mainMenu_Ptr->play.selected)
		{
			mainMenu_Ptr->play.selected = false;
			mainMenu_Ptr->exit.selected = true;
		}
		else if (mainMenu_Ptr->options.selected)
		{
			mainMenu_Ptr->options.selected = false;
			mainMenu_Ptr->play.selected = true;
		}
		else if (mainMenu_Ptr->credits.selected)
		{
			mainMenu_Ptr->credits.selected = false;
			mainMenu_Ptr->options.selected = true;
		}
		else //if(currentGameState.mainMenuButtons.exit.selected)
		{
			mainMenu_Ptr->exit.selected = false;
			mainMenu_Ptr->credits.selected = true;
		}
	}

	if (gameplay::input.enter)
	{
		mainMenu_Ptr->selectEvent = true;
		if (mainMenu_Ptr->play.selected)
		{
			gameplay::currentGameState.currentLeftScore = 0;
			gameplay::currentGameState.currentRightScore = 0;
			gameplay::currentGameState.nextGameMenu = GAME_MENU::SELECTION_SCREEN; // CHANGE
		}
		else if (mainMenu_Ptr->options.selected)
		{
			gameplay::currentGameState.nextGameMenu = GAME_MENU::OPTIONS;
		}
		else if (mainMenu_Ptr->credits.selected)
		{
			gameplay::currentGameState.nextGameMenu = GAME_MENU::CREDITS;
		}
		else //if(currentGameState.mainMenuButtons.exit.selected)
		{
			gameplay::currentGameState.gameOn = false;
		}

	}
}