#include "match.h"
#include "External Variables/extern_variables.h"
#include "Save Positions/save_Positions.h"

namespace scenes
{
    namespace match
    {

        void update() 
        {

            pauseMenuLogic();

            updatePads();

            checkAllCollisions();

            checkGoals();

            GameAudio();

            updateBall();

            acelerateBall();

            randomPowerUpControl();

        }

        void draw() 
        {

            using namespace gameplay;

            using namespace screen;

            if (currentPongObjects.padLeft.currentPowerUp != PadPowerUpStatus::NONE)
            {
                if (currentPongObjects.padLeft.currentPowerUp == PadPowerUpStatus::BIG_PAD)
                {
                    DrawText(FormatText("BIGGER PAD!"), SCREEN_WIDTH * 1 / 4 - (MeasureText(FormatText("BIGGER PAD!"), 10) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 10, BLACK);
                }
                else if (currentPongObjects.padLeft.currentPowerUp == PadPowerUpStatus::SPEED)
                {
                    DrawText(FormatText("PAD SPEED!"), SCREEN_WIDTH * 1 / 4 - (MeasureText(FormatText("PAD SPEED!"), 10) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 10, BLACK);
                }
                else
                {
                    DrawText(FormatText("SHIELD!"), SCREEN_WIDTH * 1 / 4 - (MeasureText(FormatText("SHIELD!"), 10) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 10, BLACK);
                }
            }

            if (currentPongObjects.padRight.currentPowerUp != PadPowerUpStatus::NONE)
            {
                if (currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::BIG_PAD)
                {
                    DrawText(FormatText("BIGGER PAD!"), SCREEN_WIDTH * 3 / 4 - (MeasureText(FormatText("BIGGER PAD!"), 10) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 10, BLACK);
                }
                else if(currentPongObjects.padRight.currentPowerUp == PadPowerUpStatus::SPEED)
                {
                    DrawText(FormatText("PAD SPEED!"), SCREEN_WIDTH * 3 / 4 - (MeasureText(FormatText("PAD SPEED!"), 10) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 10, BLACK);
                }
                else
                {
                    DrawText(FormatText("SHIELD!"), SCREEN_WIDTH * 3 / 4 - (MeasureText(FormatText("SHIELD!"), 10) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 10, BLACK);
                }
            }


            DrawText(FormatText("Goals per round: %01i", config::goalPerRound), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Goals per round: %01i", config::goalPerRound), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);

            DrawText(FormatText("Current Match score: %01i - %01i", currentGameState.currentRightScore, currentGameState.currentLeftScore), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Current Match score: %01i - %01i", currentGameState.currentLeftScore, currentGameState.currentRightScore), 20) / 2), SCREEN_HEIGHT * 7 / 8, 20, BLACK);

            DrawText(FormatText("Rounds score: %01i - %01i", currentGameState.currentRightRoundScore, currentGameState.currentLeftRoundScore), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Rounds score: %01i - %01i", currentGameState.currentRightRoundScore, currentGameState.currentLeftRoundScore), 20) / 2), SCREEN_HEIGHT * 15 / 16, 20, BLACK);


            switch (gameplay::currentKeycodes.down_W)
            {

            case KEY_UP:
                DrawText(FormatText("Left up: Up Arrow"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("Left up: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 6 / 8, 20, BLACK);
                break;

            case KEY_DOWN:
                DrawText(FormatText("Left up: Down Arrow"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("Left up: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 6 / 8, 20, BLACK);
                break;

            case KEY_LEFT:
                DrawText(FormatText("Left up: Left Arrow"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("Left up: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 6 / 8, 20, BLACK);
                break;

            case KEY_RIGHT:
                DrawText(FormatText("Left up: Right Arrow"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("Left up: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 6 / 8, 20, BLACK);
                break;

            default:
                DrawText(FormatText("Left up: %c", (char)gameplay::currentKeycodes.down_W), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("Left up: %c", (char)gameplay::currentKeycodes.down_W), 20) / 2), SCREEN_HEIGHT * 6 / 8, 20, BLACK);
                break;

            }

            switch (gameplay::currentKeycodes.down_S)
            {

            case KEY_UP:
                DrawText(FormatText("Left down: Up Arrow"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("Left down: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
                break;

            case KEY_DOWN:
                DrawText(FormatText("Left down: Down Arrow"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("Left down: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
                break;

            case KEY_LEFT:
                DrawText(FormatText("Left down: Left Arrow"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("Left down: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
                break;

            case KEY_RIGHT:
                DrawText(FormatText("Left down: Right Arrow"), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("Left down: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
                break;

            default:
                DrawText(FormatText("Left down: %c", (char)gameplay::currentKeycodes.down_S), SCREEN_WIDTH_EIGHT - (MeasureText(FormatText("Left down: %c", (char)gameplay::currentKeycodes.down_S), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
                break;

            }

            switch (gameplay::currentKeycodes.down_UpArrow)
            {

            case KEY_UP:
                DrawText(FormatText("Right up: Up Arrow"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("Right up: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 6 / 8, 20, BLACK);
                break;

            case KEY_DOWN:
                DrawText(FormatText("Right up: Down Arrow"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("Right up: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 6 / 8, 20, BLACK);
                break;

            case KEY_LEFT:
                DrawText(FormatText("Right up: Left Arrow"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("Right up: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 6 / 8, 20, BLACK);
                break;

            case KEY_RIGHT:
                DrawText(FormatText("Right up: Right Arrow"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("Right up: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 6 / 8, 20, BLACK);
                break;

            default:
                DrawText(FormatText("Right up: %c", (char)gameplay::currentKeycodes.down_UpArrow), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("Right up: %c", (char)gameplay::currentKeycodes.down_W), 20) / 2), SCREEN_HEIGHT * 6 / 8, 20, BLACK);
                break;

            }

            switch (gameplay::currentKeycodes.down_DownArrow)
            {

            case KEY_UP:
                DrawText(FormatText("Right down: Up Arrow"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("Right down: Up Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
                break;

            case KEY_DOWN:
                DrawText(FormatText("Right down: Down Arrow"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("Right down: Down Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
                break;

            case KEY_LEFT:
                DrawText(FormatText("Right down: Left Arrow"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("Right down: Left Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
                break;

            case KEY_RIGHT:
                DrawText(FormatText("Right down: Right Arrow"), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("Right down: Right Arrow"), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
                break;

            default:
                DrawText(FormatText("Right down: %c", (char)gameplay::currentKeycodes.down_DownArrow), SCREEN_WIDTH * 7 / 8 - (MeasureText(FormatText("Right down: %c", (char)gameplay::currentKeycodes.down_S), 20) / 2), SCREEN_HEIGHT * 13 / 16, 20, BLACK);
                break;

            }

            if (currentGameState.pause)
            {
                if (framesCounter < 30) 
                {
                    DrawText(FormatText("PAUSE MENU"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("PAUSE MENU"), 70) / 2), SCREEN_HEIGHT_QUARTER - 35, 70, BLACK);
                }
                DrawText(FormatText("Press 'P' to get back to the game"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press 'P' to get back to the game"), 20) / 2), SCREEN_HEIGHT * 7 / 16, 20, BLACK);
                DrawText(FormatText("Press 'ENTER' to get back to the main menu"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press 'ENTER' to get back to the main menu"), 20) / 2), SCREEN_HEIGHT * 4 / 8, 20, BLACK);
                DrawText(FormatText("Press 'R' to reset round scores"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press 'R' to reset round scores"), 20) / 2), SCREEN_HEIGHT * 9 / 16, 20, BLACK);
            }
            else if(currentGameState.rightRoundWon)
            {
                if(currentGameState.winAnimationCounter > 0)
                {

                    if(framesCounter < 30)
                    DrawText(FormatText("RIGHT PAD ROUND WON!"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("RIGHT PAD ROUND WON!"), 50) / 2), SCREEN_HEIGHT * 1 / 2, 50, currentPongObjects.padRight.color);

                    currentGameState.winAnimationCounter--;
                }
                else 
                {
                    currentGameState.rightRoundWon = false;
                }
            }
            else if(currentGameState.leftRoundWon)
            {
                if (currentGameState.winAnimationCounter > 0)
                {
                    if (framesCounter < 30)
                        DrawText(FormatText("LEFT PAD ROUND WON!"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("LEFT PAD ROUND WON!"), 50) / 2), SCREEN_HEIGHT * 1 / 2, 50, currentPongObjects.padLeft.color);

                    currentGameState.winAnimationCounter--;
                }
                else
                {
                    currentGameState.leftRoundWon = false;
                }
            }
            else if (!currentPongObjects.ball.active)
            {
                DrawText(FormatText("Press enter to start"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press enter to start"), 20) / 2), SCREEN_HEIGHT * 3 / 4, 20, currentPongObjects.ball.color);
            }
            else
            {
                DrawText(FormatText("Press 'P' to go to the pause menu"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Press 'P' to go to the pause menu"), 20) / 2), SCREEN_HEIGHT * 3 / 4, 20, BLACK);
                if (currentPongObjects.ball.active && config::superMode)
                {

                    DrawText(FormatText("Next random power up in: %01i", currentPongObjects.randomPowerUps.timer), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Next random power up in: %01i", currentPongObjects.randomPowerUps.timer), 10) / 2), SCREEN_HEIGHT * 1 / 16, 10, BLACK);

                    DrawText(FormatText("Next pad power up in %01i hits", currentPongObjects.padsPowerUps.hitsForNextPowerUp), SCREEN_WIDTH_HALF - (MeasureText(FormatText("Next pad power up in %01i hits", currentPongObjects.padsPowerUps.hitsForNextPowerUp), 10) / 2), SCREEN_HEIGHT * 2 / 16, 10, BLACK);

                    switch (currentPongObjects.randomPowerUps.currentType)
                    {
                    case PowerUpsRandomTypes::NO_POWERUP:
                        DrawText(FormatText("NO RANDOM POWER UP"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("NO RANDOM POWER UP"), 20) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 20, BLACK);
                        break;
                    case PowerUpsRandomTypes::MULTIBALL_POWERUP:
                        DrawText(FormatText("MULTIBALL!"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("MULTIBALL!"), 20) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 20, BLACK);
                        break;
                    case PowerUpsRandomTypes::OBSTACLES_POWERUP:
                        DrawText(FormatText("OBSTACLES!"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("OBSTACLES!"), 20) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 20, BLACK);
                        break;
                    case PowerUpsRandomTypes::INVERT_VELOCITY_POWERUP:
                        DrawText(FormatText("INVERTED VELOCITY!"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("INVERT VELOCITY!"), 20) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 20, BLACK);
                        break;
                    case PowerUpsRandomTypes::INVERT_CONTROLS_POWERUP:
                        DrawText(FormatText("INVERTED CONTROLS!"), SCREEN_WIDTH_HALF - (MeasureText(FormatText("INVERT CONTROLS!"), 20) / 2), SCREEN_HEIGHT_HALF - matchOffsetPanel / 2, 20, BLACK);
                        break;
                    default:
                        break;
                    }

                }
            }

            if (currentPongObjects.randomPowerUps.currentType == PowerUpsRandomTypes::OBSTACLES_POWERUP)
            {
                DrawRectangleRec(currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE1.rec, BLACK);
                DrawRectangleRec(currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE2.rec, BLACK);
            }

            if(currentPongObjects.padsPowerUps.leftShieldPowerUp.active)
            {
                DrawRectangleRec(currentPongObjects.padsPowerUps.leftShieldPowerUp.rec, Fade(currentPongObjects.padLeft.color, POWERUP_SHIELD_FADE_COLOR));
            }
            if(currentPongObjects.padsPowerUps.rightShieldPowerUp.active)
            {
                DrawRectangleRec(currentPongObjects.padsPowerUps.rightShieldPowerUp.rec, Fade(currentPongObjects.padRight.color, POWERUP_SHIELD_FADE_COLOR));
            }

            DrawRectangleRec(currentPongObjects.padLeft.rec, currentPongObjects.padLeft.color);
            DrawRectangleRec(currentPongObjects.padRight.rec, currentPongObjects.padRight.color);
            DrawRectangleRec(currentPongObjects.leftGoal.rec, currentPongObjects.leftGoal.color);
            DrawRectangleRec(currentPongObjects.rightGoal.rec, currentPongObjects.rightGoal.color);

            DrawRectangleLinesEx(currentPongObjects.outerBounds.rec, currentPongObjects.outerBounds.size, currentPongObjects.outerBounds.color);

            DrawCircle(static_cast<int>(currentPongObjects.ball.position.x), static_cast<int>(currentPongObjects.ball.position.y), currentPongObjects.ball.radius, currentPongObjects.ball.color);

            for (int i = 0; i < trailMaxParticlesNumber; i++)
            {
                if (currentPongObjects.ball.fullTrail[i].active)
                {
                    DrawCircle(static_cast<int>(currentPongObjects.ball.fullTrail[i].position.x), static_cast<int>(currentPongObjects.ball.fullTrail[i].position.y), currentPongObjects.ball.fullTrail[i].radius, currentPongObjects.ball.fullTrail[i].color);
                }
            }

            if (currentPongObjects.multiball.active && currentPongObjects.randomPowerUps.currentType == PowerUpsRandomTypes::MULTIBALL_POWERUP)
            {

                DrawCircle(static_cast<int>(currentPongObjects.multiball.position.x), static_cast<int>(currentPongObjects.multiball.position.y), currentPongObjects.multiball.radius, currentPongObjects.multiball.color);

            }

        }

        void init()
        {

            PlayMusicStream(audio::gameMusic.MatchMusic);
            matchConfig();
            gameplay::currentGameState.currentLeftRoundScore = LoadStorageValue(SAVE_POSITION_LEFT_SCORE);
            gameplay::currentGameState.currentRightRoundScore = LoadStorageValue(SAVE_POSITION_RIGHT_SCORE);

        }

        void deInit()
        {

            StopMusicStream(audio::gameMusic.MatchMusic);

        }

    }
}