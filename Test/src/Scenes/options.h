#ifndef OPTIONS_H
#define OPTIONS_H

#include "raylib.h"

namespace scenes
{
    namespace options
    {

        void update();

        void draw();

        void init();

        void deInit();

    }
}

#endif OPTIONS_H