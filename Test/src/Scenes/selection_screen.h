#ifndef SELECTION_SCREEN_H
#define SELECTION_SCREEN_H

#include "raylib.h"

namespace scenes
{
    namespace selection_screen
    {

        void update();

        void draw();

        void init();

        void deInit();

    }
}

#endif SELECTION_SCREEN_H