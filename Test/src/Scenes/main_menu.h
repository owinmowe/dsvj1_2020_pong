#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "raylib.h"

namespace scenes
{
    namespace main_menu
    {

        void update();

        void draw();

        void init();

        void deInit();

    }
}

#endif MAIN_MENU_H