#ifndef CREDITS_H
#define CREDITS_H

#include "raylib.h"

namespace scenes
{
    namespace credits
    {

        void update();

        void draw();

        void init();

        void deInit();

    }
}

#endif CREDITS_H