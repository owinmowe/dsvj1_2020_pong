#ifndef UI_H
#define UI_H

#include "raylib.h"

const int MAX_SIZE_TEXT = 20;

const int PAD_COLOR_SIZE = 8;

const int matchOffsetPanel = 200;

const Color padColorsArray[PAD_COLOR_SIZE]{ BLUE, RED, YELLOW, GREEN, PURPLE, ORANGE, BROWN, BLACK};

enum class KEY_CHANGE_POSITION { LEFT_UP, LEFT_DOWN, LEFT_LEFT, LEFT_RIGHT, RIGHT_UP, RIGHT_DOWN, RIGHT_LEFT, RIGHT_RIGHT, SIZE };

struct Button
{

	bool selected = false;
	//Rectangle boundary;
	//char text[MAX_SIZE_TEXT] = "";

};

struct MainMenu
{

	short titleFrames = 0;
	bool titleDrawn = false;
	bool selectEvent = false;
	bool moveEvent = false;
	Button play;
	Button options;
	Button credits;
	Button exit;

};

const int MAIN_MENU_TITLE_SPEED = 10; // Mod invertido, menor numero mayor velocidad
const char MAIN_MENU_TITLE[] = "SUPER PONG";
const char MAIN_MENU_PLAY_TEXT[] = "PLAY";
const char MAIN_MENU_OPTIONS_TEXT[] = "OPTIONS";
const char MAIN_MENU_CREDITS_TEXT[] = "CREDITS";
const char MAIN_MENU_EXIT_TEXT[] = "EXIT";

struct Circle
{
	Vector2 pos;
	int radius;
};

struct SelectionScreen
{

	bool selectEvent = false;
	bool moveEvent = false;
	Rectangle leftRec = { 0, 0, 0, 0 };
	int leftPadPosition = 0;
	Rectangle rightRec = { 0, 0, 0, 0 };
	int rightPadPosition = 0;
	bool matchConfig = false;
	Button matchTypeButton;
	Button goalsPerRoundButton;
	Button padSpeedButton;
	Button padSizeButton;
	Button ballSizeButton;
	Button hitsForPowerUpButton;
	Button timeRandomPowerUpButton;

	Rectangle padDemo{ 0,0,0,0 };
	bool padDemoDirectionDown = false;
	Circle ballDemo;

};

const char SELECTION_SCREEN_VS[] = "VS";
const char SELECTION_SCREEN_LEFT_SIDE_TEXT[] = "LEFT PAD";
const char SELECTION_SCREEN_RIGHT_SIDE_TEXT[] = "RIGHT PAD";

struct OptionScreen
{

	bool selectEvent = false;
	bool moveEvent = false;
	Button musicVolumeButton;
	bool settingMusicVolume = false;
	Button soundVolumeButton;
	bool settingSoundVolume = false;
	Button changeInputButton;
	bool settingNewKeys = false;
	KEY_CHANGE_POSITION currentKeyChangePosition;
	Button backButton;
	char currentInputToChange[MAX_SIZE_TEXT] = "";

};

const char SOUND_VOLUME_TEXT[] = "SOUND VOLUME";
const char MUSIC_VOLUME_TEXT[] = "MUSIC VOLUME";
const char CHANGE_INPUT_KEYS_TEXT[] = "CHANGE KEYS";
const char BACK_TEXT[] = "BACK";

#endif UI_H