#ifndef MENU_H
#define MENU_H

#include "raylib.h"
#include "Input/Input.h"
#include "UI/ui.h"

const Color SELECTED_COLOR = BLUE;

const Color UNSELECTED_COLOR = BLACK;

const Color LOCKED_SELECTION_COLOR = GRAY;

struct GameMenus
{

	MainMenu mainMenu;

	SelectionScreen selectionScreen;

	OptionScreen optionScreen;

};

void changeVolume(bool& moveEvent, bool& selectEvent, bool& changingSound, float& volume);

void pauseMenuLogic();

#endif MENU_H