#include "UI/menu.h"
#include "Gameplay/game_logic.h"
#include "External Variables/Extern_Variables.h"

void changeVolume(bool& moveEvent, bool& selectEvent, bool& changingSound, float& volume)
{

	using namespace gameplay;

	if (input.enter)
	{

		selectEvent = true;
		changingSound = false;
		return;

	}

	if((input.pressed_A || input.pressed_LeftArrow) && volume > 0.1)
	{
		moveEvent = true;
		volume -= (float)0.1;
		audio::changeVolume = true;
	}
	else if((input.pressed_D || input.pressed_RightArrow) && volume < 1)
	{
		moveEvent = true;
		volume += (float)0.1;
		audio::changeVolume = true;
	}

}

void pauseMenuLogic()
{
	if (gameplay::input.pause && gameplay::currentPongObjects.ball.active) { gameplay::currentGameState.pause = !gameplay::currentGameState.pause; }

	if (gameplay::currentGameState.pause)
	{
		if (gameplay::input.enter)
		{
			gameplay::currentGameState.nextGameMenu = GAME_MENU::MAIN_MENU;
			gameplay::currentGameState.pause = false;
		}
		else if(gameplay::input.reset)
		{
			resetScore();
		}
	}
}