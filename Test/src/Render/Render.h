#ifndef RENDER_H
#define RENDER_H

#include "Scenes/main_menu.h"
#include "Scenes/credits.h"
#include "Scenes/match.h"
#include "Scenes/options.h"
#include "Scenes/selection_screen.h"

void draw();

#endif RENDER_H
