#include "render.h"
#include "External Variables/extern_variables.h"

void draw()
{
    BeginDrawing();

    ClearBackground(RAYWHITE);

    switch (gameplay::currentGameState.currentGameMenu)
    {

    case GAME_MENU::MAIN_MENU:

        scenes::main_menu::draw();

        break;

    case GAME_MENU::OPTIONS:

        scenes::options::draw();

        break;

    case GAME_MENU::CREDITS:

        scenes::credits::draw();

        break;

    case GAME_MENU::SELECTION_SCREEN:

        scenes::selection_screen::draw();

        break;

    case GAME_MENU::IN_MATCH:

        scenes::match::draw();

        break;

    default:
        break;
    }

    EndDrawing();

}