#ifndef EXTERN_VARIABLES_H
#define EXTERN_VARIABLES_H

#include "Audio/Audio.h"
#include "UI/Menu.h"
#include "Gameplay/PongObjects/Pong_Objects.h"
#include "Input/Input.h"
#include "Gameplay/game_logic.h"

namespace screen
{

	const int SCREEN_WIDTH = 1200;

	const int SCREEN_WIDTH_HALF = SCREEN_WIDTH / 2;

	const int SCREEN_WIDTH_QUARTER = SCREEN_WIDTH / 4;

	const int SCREEN_WIDTH_EIGHT = SCREEN_WIDTH / 8;


	const int SCREEN_HEIGHT = 650;

	const int SCREEN_HEIGHT_HALF = SCREEN_HEIGHT / 2;

	const int SCREEN_HEIGHT_QUARTER = SCREEN_HEIGHT / 4;

	const int SCREEN_HEIGHT_EIGHT = SCREEN_HEIGHT / 8;


	const int FRAMES_PER_SECOND = 60;

}

namespace gameplay
{

	extern int framesCounter;

	extern Input input;

	extern NEW_KEYCODES currentKeycodes;

	extern GameState currentGameState;

	extern GameMenus currentGameMenus;

	extern PongObjects currentPongObjects;

}

namespace config
{

	extern bool superMode;

	extern int padSpeed;

	extern int padSize;

	extern int ballSize;

	extern int hitsForPowerUp;

	extern int timeForRandomPowerUp;

	extern int goalPerRound;

}

namespace audio
{

	extern GameMusic gameMusic;

	extern GameSounds gameSounds;

	extern bool changeVolume;

}

#endif EXTERN_VARIABLES_H