#include "Collision.h"
#include "External Variables/Extern_Variables.h"

void checkAllCollisions()
{

    using namespace gameplay;

    resetGoalEvents(currentPongObjects.leftGoal, currentPongObjects.rightGoal);

    padsBoundCollision(currentPongObjects.padLeft, currentPongObjects.padRight, currentPongObjects.outerBounds);
    
    ballOuterBoundsCollision(currentPongObjects.ball, currentPongObjects.outerBounds);

    allPadsBallCollision(currentPongObjects.ball, currentPongObjects.padLeft, currentPongObjects.padRight);

    allBallGoalsCollisions(currentPongObjects.ball, currentPongObjects.leftGoal, currentPongObjects.rightGoal);

    ballPowerUpsCollision(currentPongObjects.ball);

    if (currentPongObjects.multiball.active)
    {

        ballOuterBoundsCollision(currentPongObjects.multiball, currentPongObjects.outerBounds);

        ballPowerUpsCollision(currentPongObjects.multiball);

        allPadsBallCollision(currentPongObjects.multiball, currentPongObjects.padLeft, currentPongObjects.padRight);

        allBallGoalsCollisions(currentPongObjects.multiball, currentPongObjects.leftGoal, currentPongObjects.rightGoal);
    }

}

void resetGoalEvents(Bound& leftGoal, Bound& rightGoal)
{

    if (leftGoal.hitEvent || rightGoal.hitEvent)
    {
        leftGoal.hitEvent = false;
        rightGoal.hitEvent = false;
    }

}

void allBallGoalsCollisions(Ball& ball, Bound& leftGoal, Bound& rightGoal)
{

    if (CheckCollisionCircleRec(ball.position, ball.radius, leftGoal.rec))
    {

        ballGoalCollision(ball, leftGoal);

    }
    else if (CheckCollisionCircleRec(ball.position, ball.radius, rightGoal.rec) && ball.active)
    {

        ballGoalCollision(ball, rightGoal);

    }

}

void ballGoalCollision(Ball& ball, Bound& goal)
{

    goal.hitEvent = true;
    ball.goalEvent = true;

}

void ballOuterBoundsCollision(Ball& ball, Bound& outerBounds)
{
    if (ball.position.y >= outerBounds.rec.height - (outerBounds.size + ball.radius) || ball.position.y <= 0 + (outerBounds.size + ball.radius))
    {
        ball.currentVelocity.y = -ball.currentVelocity.y;
        ball.padSideCollision = false;
    }
}

void allPadsBallCollision(Ball& ball, Pad& padLeft, Pad& padRight)
{

    if (ball.hitEvent) { ball.hitEvent = false; }

    if (CheckCollisionCircleRec(ball.position, ball.radius, padLeft.rec) && !ball.padSideCollision && ball.currentVelocity.x < 0)
    {

        ball.hitEvent = true;

        if (ball.position.x + ball.radius > padLeft.rec.x + padLeft.rec.width)
        {
            if(gameplay::currentPongObjects.randomPowerUps.currentType == PowerUpsRandomTypes::INVERT_VELOCITY_POWERUP)
            {
                ball.currentVelocity.y = -ball.currentVelocity.y; //(ball.position.x - padLeft.rec.x) / (player.size.x / 2) * 5;
            }
            ball.currentVelocity.x = -ball.currentVelocity.x;
        }
        else
        {
            ball.currentVelocity.y = -ball.currentVelocity.y;
            ball.padSideCollision = true;
        }
        ball.color = padLeft.color;
    }
    else if (CheckCollisionCircleRec(ball.position, ball.radius, padRight.rec) && !ball.padSideCollision && ball.currentVelocity.x > 0)
    {

        ball.hitEvent = true;

        if (ball.position.x - ball.radius < padRight.rec.x)
        {
            if (gameplay::currentPongObjects.randomPowerUps.currentType == PowerUpsRandomTypes::INVERT_VELOCITY_POWERUP)
            {
                ball.currentVelocity.y = -ball.currentVelocity.y;
            }
            ball.currentVelocity.x = -ball.currentVelocity.x;
        }
        else
        {
            ball.currentVelocity.y = -ball.currentVelocity.y;
            ball.padSideCollision = true;
        }
        ball.color = padRight.color;
    }

}

void padsBoundCollision(Pad& padLeft, Pad& padRight, Bound outerBounds)
{

    if (padLeft.rec.y < 0 + outerBounds.size)
    {
        padLeft.rec.y = static_cast<float>(outerBounds.size);
    }
    if (padLeft.rec.y + padLeft.rec.height > outerBounds.rec.height - outerBounds.size)
    {
        padLeft.rec.y = screen::SCREEN_HEIGHT - matchOffsetPanel - outerBounds.size - padLeft.rec.height;
    }
    if (padRight.rec.y < 0 + outerBounds.size)
    {
        padRight.rec.y = static_cast<float>(outerBounds.size);
    }
    if (padRight.rec.y + padRight.rec.height > outerBounds.rec.height - outerBounds.size)
    {
        padRight.rec.y = screen::SCREEN_HEIGHT - matchOffsetPanel - outerBounds.size - padRight.rec.height;
    }

}

void ballPowerUpsCollision(Ball& ball)
{

    Obstacle* obstacle1 = &gameplay::currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE1;

    Obstacle* obstacle2 = &gameplay::currentPongObjects.randomPowerUps.POWERUP_RANDOM_OBSTACLE2;

    if(gameplay::currentPongObjects.randomPowerUps.currentType == PowerUpsRandomTypes::OBSTACLES_POWERUP)
    {

        if(CheckCollisionCircleRec(ball.position, ball.radius, obstacle1->rec))
        {
            if (obstacle1->colided) { return; }
            obstacle1->colided = true;

        }
        else if(CheckCollisionCircleRec(ball.position, ball.radius, obstacle2->rec))
        {
            if (obstacle2->colided) { return; }
            obstacle2->colided = true;

        }
        else
        {
            obstacle1->colided = false;
            obstacle2->colided = false;
        }

        if(obstacle1->colided)
        {

            if (ball.position.y - ball.currentVelocity.y > obstacle1->rec.y + obstacle1->rec.height || ball.position.y - ball.currentVelocity.y < obstacle1->rec.y)
            {
                ball.currentVelocity.y = -ball.currentVelocity.y;
            }
            else
            {
                ball.currentVelocity.x = -ball.currentVelocity.x;
            }

        }
        else if(obstacle2->colided)
        {

            if (ball.position.y - ball.currentVelocity.y > obstacle2->rec.y + obstacle2->rec.height || ball.position.y - ball.currentVelocity.y < obstacle2->rec.y)
            {
                ball.currentVelocity.y = -ball.currentVelocity.y;
            }
            else
            {
                ball.currentVelocity.x = -ball.currentVelocity.x;
            }
        }

    }
    
    Shield* leftShield_Ptr = &gameplay::currentPongObjects.padsPowerUps.leftShieldPowerUp;
    Shield* rightShield_Ptr = &gameplay::currentPongObjects.padsPowerUps.rightShieldPowerUp;

    if(leftShield_Ptr->active)
    {
        if (CheckCollisionCircleRec(ball.position, ball.radius, leftShield_Ptr->rec))
        {
            leftShield_Ptr->active = false;
            ball.currentVelocity.x = -ball.currentVelocity.x;
        }
    }
    if (rightShield_Ptr->active)
    {
        if (CheckCollisionCircleRec(ball.position, ball.radius, rightShield_Ptr->rec))
        {
            rightShield_Ptr->active = false;
            ball.currentVelocity.x = -ball.currentVelocity.x;
        }
    }


}