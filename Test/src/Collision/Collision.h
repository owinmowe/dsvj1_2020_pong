#ifndef COLLISION_H
#define COLLISION_H

#include "Gameplay/PongObjects/Pong_Objects.h"

void checkAllCollisions();

void allBallGoalsCollisions(Ball& ball, Bound& leftGoal, Bound& rightGoal);

void ballOuterBoundsCollision(Ball& ball, Bound& outerBounds);

void allPadsBallCollision(Ball& ball, Pad& padLeft, Pad& padRight);

void padsBoundCollision(Pad& padLeft, Pad& padRight, Bound outerBounds);

void ballGoalCollision(Ball& ball, Bound& goal);

void ballPowerUpsCollision(Ball& ball);

void resetGoalEvents(Bound& leftGoal, Bound& rightGoal);

#endif COLLISION_H